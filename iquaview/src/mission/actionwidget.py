"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
Action widget to configure in options menu
"""

import logging


from iquaview_lib.ui.ui_action import Ui_ActionWidget

from qgis.gui import QgsCollapsibleGroupBox

logger = logging.getLogger(__name__)


class ActionWidget(QgsCollapsibleGroupBox, Ui_ActionWidget):
    def __init__(self, title="Action", action_id=None, id=None, description=None, collapsed=False,  parent=None):
        """
        Constructor
        """
        super(ActionWidget, self).__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(id))
        self.setWindowTitle("ActionWidget")
        self.setTitle(title)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()

        self.action_id_lineEdit.setText(action_id)
        self.description_lineEdit.setText(description)
        self.action_name_lineEdit.setText(title)
        self.action_name_lineEdit.textChanged.connect(self.setTitle)
