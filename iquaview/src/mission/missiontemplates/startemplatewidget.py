"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Star pattern definition
"""

import math

from PyQt5.QtWidgets import QWidget, QMessageBox
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt

from iquaview.src.ui.ui_star_templatewidget import Ui_StarTemplateWidget
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionSection,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH)
from iquaview_lib.utils.calcutils import (distance_ellipsoid,
                                          endpoint_sphere,
                                          get_angle_of_line_between_two_points,
                                          wrap_angle)

from iquaview.src.mission.maptools.polygontools import (PolygonBy3PointsTool,
                                                        PolygonFromCenterTool,
                                                        PolygonFromCenterFixedTool,
                                                        PolygonByFixedExtentTool)

from qgis.gui import QgsRubberBand
from qgis.core import (QgsPointXY,
                       QgsWkbTypes,
                       QgsDistanceArea,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransform,
                       QgsGeometry)


class StarTemplateWidget(QWidget, Ui_StarTemplateWidget):

    def __init__(self, canvas, msglog, current_missiontrack, parent=None):
        """
        Init of the object StarTemplateWidget
        :param canvas: Canvas to draw objects
        :type canvas: QgsMapCanvas
        :param msglog: Log to write messages
        :type msglog: QgsMessageLog
        :param current_missiontrack: Mission track to add new waypoints from templates
        :type current_missiontrack: MissionTrack
        """
        super(StarTemplateWidget, self).__init__(parent)
        self.setupUi(self)
        self.canvas = canvas
        self.msglog = msglog
        self.current_missiontrack = current_missiontrack
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.rubber_band = None
        self.rubber_band_points = None
        self.initial_point = None

        self.template_type = 'star_template'
        self.wp = list()
        self.mission = None
        crs = QgsCoordinateReferenceSystem("EPSG:4326")
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())

        self.wp_list = []

        self.rubber_band = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
        self.rubber_band.setWidth(2)
        self.rubber_band.setColor(QColor("green"))

        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setColor(QColor("green"))
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)

        self.initial_point = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.initial_point.setColor(QColor("red"))
        self.initial_point.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.initial_point.setIconSize(10)

        self.onTarget = False
        self.area_points = None
        self.missionAreaDefined = False
        self.previewing = False

        # Initialize tools and signals
        # Draw star not fixed tool
        self.star_by_3_points_tool = PolygonBy3PointsTool(self.canvas, self.number_points_spinBox.value(), "star")
        self.star_by_3_points_tool.message_signal.connect(self.pass_to_message_bar)
        self.star_by_3_points_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.star_by_3_points_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw star from center not fixed tool
        self.star_from_center_tool = PolygonFromCenterTool(self.canvas, self.number_points_spinBox.value(), "star")
        self.star_from_center_tool.message_signal.connect(self.pass_to_message_bar)
        self.star_from_center_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.star_from_center_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw star from center fixed size tool
        self.star_from_center_fixed_tool = PolygonFromCenterFixedTool(self.canvas, 0.0, 0.0,
                                                                         self.number_points_spinBox.value(), "star")
        self.star_from_center_fixed_tool.message_signal.connect(self.pass_to_message_bar)
        self.star_from_center_fixed_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.star_from_center_fixed_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw star fixed size tool
        self.star_by_fixed_extent_tool = PolygonByFixedExtentTool(self.canvas, 0.0, 0.0,
                                                                     self.number_points_spinBox.value(), "star")
        self.star_by_fixed_extent_tool.message_signal.connect(self.pass_to_message_bar)
        self.star_by_fixed_extent_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.star_by_fixed_extent_tool.geometry_finished_signal.connect(self.create_mission_area)

        self.drawRectangleButton.clicked.connect(self.draw_mission_area)
        self.centerOnTargetButton.clicked.connect(self.draw_mission_area)

        self.alongTrackLabel.setEnabled(False)
        self.acrossTrackLabel.setEnabled(False)
        self.alongTLength.setEnabled(False)
        self.alongTLength.setEnabled(False)
        self.acrossTLength.setEnabled(False)

        self.alongTLength.valueChanged.connect(self.star_from_center_fixed_tool.set_x_length)
        self.acrossTLength.valueChanged.connect(self.star_from_center_fixed_tool.set_y_length)
        self.alongTLength.valueChanged.connect(self.star_by_fixed_extent_tool.set_x_length)
        self.acrossTLength.valueChanged.connect(self.star_by_fixed_extent_tool.set_y_length)

        self.fixedExtent.toggled.connect(self.fixed_extend_toggled)

        self.number_points_spinBox.valueChanged.connect(self.update_segments_to_tools)
        self.number_points_spinBox.valueChanged.connect(self.update_initial_point_spinBox)
        self.initial_point_spinBox.valueChanged.connect(self.change_initial_point)

        self.heave_mode_widget.on_heave_mode_box_changed(0)
        self.update_initial_point_spinBox()

    def get_template_mission(self):
        """
        Returns a mission that contains the waypoints in the template
        :return: The mission template
        :rtype: Mission
        """
        if self.previewing:
            return self.mission
        else:
            return None

    def fixed_extend_toggled(self):
        """
        Enables or disables along and across track options depending on
        whether the fixed extent box is checked or not
        """
        is_checked = self.fixedExtent.isChecked()
        self.alongTrackLabel.setEnabled(is_checked)
        self.acrossTrackLabel.setEnabled(is_checked)
        self.alongTLength.setEnabled(is_checked)
        self.acrossTLength.setEnabled(is_checked)

    def pass_to_message_bar(self, msg):
        """
        Passes the message to the message bar
        :param msg: message to pass
        :type msg: str
        """
        self.msglog.logMessage("")
        self.msglog.logMessage(msg, "Star Template", 0)

    def draw_mission_area(self):
        """
        Called when a tool is selected.
        Changes tools and clears previous drawings and variables from the other tool
        """
        self.onTarget = False
        sender = self.sender().objectName()
        if not self.missionAreaDefined:
            if self.automaticExtent.isChecked():
                if sender == self.drawRectangleButton.objectName():
                    self.msglog.logMessage("Click starting point", "Star Template", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.star_by_3_points_tool)
                elif sender == self.centerOnTargetButton.objectName():
                    self.onTarget = True
                    self.msglog.logMessage("Click center point", "Star Template", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.star_from_center_tool)
                self.missionAreaDefined = True
                return

            elif self.fixedExtent.isChecked():
                if self.alongTLength.value() != 0.0 and self.acrossTLength.value() != 0.0:
                    if sender == self.drawRectangleButton.objectName():
                        self.msglog.logMessage("Click starting point", "Star Template", 0)

                        self.canvas.setMapTool(self.star_by_fixed_extent_tool)
                        self.missionAreaDefined = True
                    elif sender == self.centerOnTargetButton.objectName():
                        self.onTarget = True
                        self.msglog.logMessage("Click center point", "Star Template", 0)
                        self.canvas.setMapTool(self.star_from_center_fixed_tool)
                    return

                else:
                    QMessageBox.warning(None,
                                        "Mission Template",
                                        "<center>Track lengths must be different from zero. </center>",
                                        QMessageBox.Close)

                    return
        else:

            self.deactivate_tool()

            # Reset every rubber band and mission object
            self.reset_preview_data()
            self.update_initial_point_spinBox()
            self.draw_mission_area()

    def clear_initial_point(self):
        """
        Reset the initial point rubber band from canvas
        """
        if self.initial_point is not None:
            self.initial_point.reset(QgsWkbTypes.PointGeometry)

    def reset_preview_data(self):
        """ Resets rubber bands and objects containing area info """
        self.clear_initial_point()

        if self.rubber_band is not None:
            self.rubber_band.reset(QgsWkbTypes.LineGeometry)

        if self.rubber_band_points is not None:
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

        self.missionAreaDefined = False
        self.previewing = False
        self.area_points = None
        self.mission = None

    def change_initial_point(self):
        """
        Change the initial point
        """
        self.clear_initial_point()
        if self.area_points is not None:
            area = self.area_points
            index = self.initial_point_spinBox.value()
            if index > self.number_points_spinBox.value()*2:
                index = self.number_points_spinBox.value()
            index = index - 1  # numbers start at 1 but our lists at 0

            trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem("EPSG:4326"),
                                                      self.canvas.mapSettings().destinationCrs(),
                                                      QgsProject.instance().transformContext())
            init_point = trans_wgs_to_map.transform(area[index].x(), area[index].y())

            self.initial_point.addPoint(init_point)

    def create_mission_area(self, geom):
        """
        Creates a mission area with the geometry specified
        :param geom: geometry of the area
        :type geom: QgsGeometry
        """
        self.pass_to_message_bar("")
        self.clear_initial_point()
        if geom is not None:
            # Store points to variables
            self.area_points = list()
            for vertex in geom.vertices():
                self.area_points.append(QgsPointXY(vertex))

            # We need all the individual points, but the geometry is closed and has the first point also at the end
            self.area_points = self.area_points[:-1]

            self.change_initial_point()

            self.missionAreaDefined = True

            if self.rubber_band is not None and self.rubber_band_points is not None:
                transparent_green = QColor(0, 128, 0, 64)
                self.rubber_band.setColor(transparent_green)
                self.rubber_band_points.setColor(transparent_green)

    def preview_tracks(self):
        """ preview tracks on the canvas"""
        if self.missionAreaDefined and self.area_points is not None:
            if ((self.heave_mode_widget.get_heave_mode() == HEAVE_MODE_ALTITUDE
                 or self.heave_mode_widget.get_heave_mode() == HEAVE_MODE_BOTH)
                    and self.heave_mode_widget.get_altitude() == 0):
                QMessageBox.warning(None,
                                    "Mission Template",
                                    "<center>Altitude must be different from zero. </center>",
                                    QMessageBox.Close)
            else:
                self.wp_list = self.compute_tracks(self.get_area_points())
                self.track_to_mission(self.wp_list, self.heave_mode_widget.get_depth(),
                                      self.heave_mode_widget.get_altitude(),
                                      self.heave_mode_widget.get_heave_mode(),
                                      self.get_speed(),
                                      self.get_xy_tolerance(),
                                      self.get_no_altitude_goes_up())

                # show rubber band with temporal tracks
                self.rubber_band.reset(QgsWkbTypes.LineGeometry)
                self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                self.change_initial_point()
                for wp in self.wp_list:
                    trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem("EPSG:4326"),
                                                              self.canvas.mapSettings().destinationCrs(),
                                                              QgsProject.instance().transformContext())
                    rb_wp = trans_wgs_to_map.transform(wp.x(), wp.y())
                    self.rubber_band.addPoint(QgsPointXY(rb_wp.x(), rb_wp.y()))
                    self.rubber_band_points.addPoint(QgsPointXY(rb_wp.x(), rb_wp.y()))
                self.rubber_band_points.show()
                self.rubber_band.show()

                self.previewing = True

                self.rubber_band.setColor(QColor("green"))
                self.rubber_band_points.setColor(QColor("green"))

                self.hide_map_tool_bands()

        else:
            QMessageBox.warning(None,
                                "Mission Template",
                                "<center>Define first an area for the mission. </center>",
                                QMessageBox.Close)

    def get_area_points(self):
        """
        :return: The list of points of the defined mission area
        :rtype: list
        """
        return self.area_points

    def get_speed(self):
        """
        :return: Value of the speed box
        :rtype: float
        """
        return float(self.speed_doubleSpinBox.text())

    def get_xy_tolerance(self):
        """
        :return: Value of the tolerance in the x and y direction
        :rtype: float
        """
        return float(self.xy_tolerance_doubleSpinBox.text())

    def get_no_altitude_goes_up(self):
        return self.no_altitude_comboBox.currentText() == "Go up"

    def get_mission_type(self):
        """
        :return: Type of the mission
        :rtype: str
        """
        return self.template_type

    def compute_tracks(self, area_points):

        """
        Compute star tracks
        :param area_points: points defining the extent of the tracks, they should be in WGS 84 lat/lon.
                            first two points define the along track direction.
        :return: list of ordered waypoints of star trajectory
        """
        new_area = list()
        index = self.initial_point_spinBox.value() - 1
        geom = QgsGeometry.fromPolygonXY([area_points])
        center_point = geom.centroid().vertexAt(0)
        angle_one = get_angle_of_line_between_two_points(center_point.x(), center_point.y(), geom.vertexAt(0).x(), geom.vertexAt(0).y(), "radians")

        radius = distance_ellipsoid(center_point.x(), center_point.y(), area_points[0].x(),area_points[0].y())
        # Compute coverage points
        angle = math.radians(90) - wrap_angle(angle_one)
        angles = []
        for i in range(self.number_points_spinBox.value()):
            angles.append(angle)
            angle += math.pi / float(self.number_points_spinBox.value())

        #roll list
        angles = [angle + (index * math.pi / float(self.number_points_spinBox.value())) for angle in angles]

        for angle in angles:
            lon_1, lat_1 = endpoint_sphere(center_point.x(), center_point.y(), radius, math.degrees(angle))
            lon_2, lat_2 = endpoint_sphere(center_point.x(), center_point.y(), radius, 180 + math.degrees(angle))

            p1 = QgsPointXY(lon_1, lat_1)
            p2 = QgsPointXY(lon_2,lat_2)
            new_area.append(p1)
            new_area.append(p2)
            new_area.append(p1)

        return new_area

    def update_initial_point_spinBox(self):
        """
        Updates the maximum initial point spinBox value with the total number of vertexes
        """
        segments = self.number_points_spinBox.value()
        if self.initial_point_spinBox.value() > segments:
            self.initial_point_spinBox.setValue(segments)
        self.initial_point_spinBox.setMaximum(segments*2)

    def track_to_mission(self, wp_list, depth, altitude, heave_mode, speed, tolerance_xy, no_altitude_goes_up):

        """
        Creates a misison with a list of waypoints and the various parameters of a mission
        :param wp_list: Waypoints of the mission
        :type wp_list: list
        :param depth: depth of the mission
        :type depth: float
        :param altitude: Altitude of the mission
        :type altitude: float
        :param heave_mode: heave mode of the mission (depth, altitude, both)
        :type heave_mode: int
        :param speed: speed of the mission
        :type speed: float
        :param tolerance_xy: tolerance in the x direction
        :type tolerance_xy: float
        :param no_altitude_goes_up: reaction of auv when no altitude (Go up, Ignore)
        :type no_altitude_goes_up: bool

        """
        self.mission = Mission()
        for wp in range(len(wp_list)):
            if wp == 0:
                # first step type waypoint
                step = MissionStep()
                mwp = MissionGoto(wp_list[wp].y(), wp_list[wp].x(), depth,  altitude,
                                  heave_mode,
                                  speed,
                                  tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
            else:
                # rest of steps type section
                step = MissionStep()
                mwp = MissionSection(wp_list[wp - 1].y(), wp_list[wp - 1].x(), depth,
                                     wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                     heave_mode,
                                     speed,
                                     tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)

    def delete_widget(self):
        """ Deletes widgets, layouts and itself """
        self.delete_all(self.layout())
        self.deleteLater()
        self.close()

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())

    def unset_map_tool(self):
        """
        Unset map tool from canvas.
        """
        if self.automaticExtent.isChecked():
            if self.onTarget:
                self.canvas.unsetMapTool(self.star_from_center_tool)
            else:
                self.canvas.unsetMapTool(self.star_by_3_points_tool)
        if self.fixedExtent.isChecked():
            if self.onTarget:
                self.canvas.unsetMapTool(self.star_from_center_fixed_tool)
            else:
                self.canvas.unsetMapTool(self.star_by_fixed_extent_tool)

    def update_segments_to_tools(self, segments):
        """
        Sets the number of segments for star tools to the provided value
        :param segments: number of segments
        :type segments: int
        """
        self.star_by_3_points_tool.set_segments(segments)
        self.star_from_center_tool.set_segments(segments)
        self.star_by_fixed_extent_tool.set_segments(segments)
        self.star_from_center_fixed_tool.set_segments(segments)

    def hide_map_tool_bands(self):
        """
        Hides maptool's bands.
        """
        if self.canvas.mapTool() == self.star_by_3_points_tool:
            self.star_by_3_points_tool.hide_bands()
        elif self.canvas.mapTool() == self.star_from_center_tool:
            self.star_from_center_tool.hide_bands()
        elif self.canvas.mapTool() == self.star_by_fixed_extent_tool:
            self.star_by_fixed_extent_tool.hide_bands()
        elif self.canvas.mapTool() == self.star_from_center_fixed_tool:
            self.star_from_center_fixed_tool.hide_bands()

    def deactivate_tool(self):
        """
        Deactivate tool.
        """
        if self.star_by_3_points_tool:
            self.star_by_3_points_tool.deactivate()
        elif self.star_by_fixed_extent_tool:
            self.star_by_fixed_extent_tool.deactivate()
        if self.star_from_center_tool:
            self.star_from_center_tool.deactivate()
        elif self.star_from_center_fixed_tool:
            self.star_from_center_fixed_tool.deactivate()

    def close(self):
        """ Deactivates tools, removes rubber bands and closes the widget """

        self.unset_map_tool()
        self.deactivate_tool()

        if self.rubber_band is not None:
            self.canvas.scene().removeItem(self.rubber_band)
            del self.rubber_band
            self.rubber_band = None
        if self.rubber_band_points is not None:
            self.canvas.scene().removeItem(self.rubber_band_points)
            del self.rubber_band_points
            self.rubber_band_points = None
        if self.initial_point is not None:
            self.canvas.scene().removeItem(self.initial_point)
            del self.initial_point
            self.initial_point = None
