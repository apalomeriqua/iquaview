"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Provides functions to check the mission status by decoding the error byte that comes
 either from WiFi (checking the /cola2_safety/status_code) or from the USBL messages.
"""

# from iquaview.src.iquaview_evologics_usbl.iquaview_evologics_usbl.evologics_lib.usbl_commands import USBLCommands
# from iquaview_evologics_usbl.evologics_lib.usbl_commands import USBLCommands
import logging
from PyQt5.QtCore import pyqtSignal, QObject, QTimer

logger = logging.getLogger(__name__)

class StatusCode:
    """
    Class to decodify the status code represented in 32bits (4 bytes)
    into different bits representing vehicle status, errors and warnings.
    """

    """
    Definitions of each bit number 
    """

    # 31-24 TBD

    # 23-18 TBD (6 bits left)

    RA_EMERGENCY_SURFACE = 17
    RA_ABORT_SURFACE = 16
    RA_ABORT_MISSION = 15
    LOW_BATTERY_WARNING = 14
    BATTERY_ERROR = 13
    WATCHDOG_TIMER = 12
    NAVIGATION_ERROR = 11
    NO_ALTITUDE_ERROR = 10
    WATER_INSIDE = 9
    HIGH_TEMPERATURE = 8
    CURRENT_MISSION_STEPS_BASE = 7  # to 0

    def __init__(self):
        pass

    def unpack(self, status_code):
        """
        Unpacks an status code integer to a dictionary of status, errors and warnings
        """
        ra_emergency_surface = (status_code & (1 << StatusCode.RA_EMERGENCY_SURFACE) != 0)
        ra_abort_surface = (status_code & (1 << StatusCode.RA_ABORT_SURFACE) != 0)
        ra_abort_mission = (status_code & (1 << StatusCode.RA_ABORT_MISSION) != 0)
        low_battery_warning = (status_code & (1 << StatusCode.LOW_BATTERY_WARNING) != 0)
        battery_error = (status_code & (1 << StatusCode.BATTERY_ERROR) != 0)
        watchdog_timer = (status_code & (1 << StatusCode.WATCHDOG_TIMER) != 0)
        navigation_error = (status_code & (1 << StatusCode.NAVIGATION_ERROR) != 0)
        no_altitude_error = (status_code & (1 << StatusCode.NO_ALTITUDE_ERROR) != 0)
        water_inside = (status_code & (1 << StatusCode.WATER_INSIDE) != 0)
        high_temperature = (status_code & (1 << StatusCode.HIGH_TEMPERATURE) != 0)
        current_mission_steps_base = (status_code & 255) #last 8 bits (1111 1111)

        return {
            'ra_emergency_surface': ra_emergency_surface,
            'ra_abort_surface': ra_abort_surface,
            'ra_abort_mission': ra_abort_mission,
            'low_battery_warning': low_battery_warning,
            'battery_error': battery_error,
            'watchdog_timer': watchdog_timer,
            'navigation_error': navigation_error,
            'no_altitude_error': no_altitude_error,
            'water_inside': water_inside,
            'high_temperature': high_temperature,
            'current_mission_steps_base': current_mission_steps_base
        }


class MissionStatus(QObject):
    mission_started = pyqtSignal()
    mission_stopped = pyqtSignal()

    def __init__(self, vehicledata, msglog):
        super(MissionStatus, self).__init__()
        # self.config = config
        self.vehicle_data = vehicledata
        self.msglog = msglog
        self.mission_sts_topic = None
        self.status = ""
        self.status_color = 'green'
        self.recovery_action_status = ""
        self.status_decoder = StatusCode()
        self.mission_is_executing = False
        self.connected = False
        self.timer = QTimer()
        self.timer.timeout.connect(self.check_mission_status)

        self.vehicle_data.state_signal.connect(self.set_vehicle_data_status)

    def init_mission_status_wifi(self):

        self.connected = True
        self.timer.start(1000)

    def set_vehicle_data_status(self):
        self.status = "No connection with COLA2"
        self.status_color = 'red'

    def check_mission_status(self):
        if self.connected:
            status_code = self.vehicle_data.get_status_code()
            recovery_action = self.vehicle_data.get_recovery_action()
            if status_code is not None:
                self.assign_status(status_code)

            if recovery_action is not None:
                self.assign_recovery_status(recovery_action)

    def assign_status(self, status_byte):

        status_code = self.status_decoder.unpack(status_byte)
        if status_code['current_mission_steps_base'] != 0:
            self.status = "Executing mission, waypoint {}".format(status_code['current_mission_steps_base'])
            self.status_color = 'green'
            self.mission_is_executing = True
            self.mission_started.emit()

            feedback = self.vehicle_data.get_captain_state_feedback()
            if self.vehicle_data.get_captain_state_feedback() is not None:
                if feedback is not None and feedback['valid_data'] == 'new_data':

                    for keyvalue in feedback['keyvalues']:
                        if keyvalue['key'] == 'total_steps':
                            self.status = self.status + "/" + str(keyvalue['value'])
        else:
            self.status = ""
            self.mission_is_executing = False
            self.mission_stopped.emit()

        if status_code['ra_emergency_surface']:
            self.status = "Recovery action: Emergency Surface!"
            self.status_color = 'red'

        if status_code['ra_abort_surface']:
            self.status = "Recovery action: Abort and Surface!"
            self.status_color = 'red'
        if status_code['ra_abort_mission']:
            self.status = "Recovery action: Abort mission!"
            self.status_color = 'red'

        if status_code['low_battery_warning']:
            self.status = "Low battery charge"
            self.status_color = 'orange'

        if status_code['battery_error']:
            self.status = "Battery charge/voltage below safety threshold"
            self.status_color = 'red'

        if status_code['watchdog_timer']:
            self.status = "Watchdog timeout reached!"
            self.status_color = 'red'

        if status_code['no_altitude_error']:
            self.status = "No altitude!"
            self.status_color = 'red'

        if status_code['high_temperature']:
            self.status = "High Temperature!"
            self.status_color = 'red'

        if status_code['navigation_error']:
            self.status = "Navigation error"
            self.status_color = 'red'

        if status_code['water_inside']:
            self.status = "Water inside!"
            self.status_color = 'red'

        if self.status != "":
            if self.status_color == 'red':
                self.msglog.logMessage("", "Status code:", 4)
                self.msglog.logMessage(self.status, "Status code:", 4)
                logger.info("Status code: {} {}".format(status_code, status_byte))
            elif self.status_color == 'orange':
                self.msglog.logMessage("", "Status code:", 3)
                self.msglog.logMessage(self.status, "Status code:", 3)
                logger.info("Status code: {} {}".format(status_code, status_byte))

    def assign_recovery_status(self, recovery_status):

        error_level = recovery_status[0]
        error_string = recovery_status[1]
        if error_level == 2:
            self.recovery_action_status = "Abort mission: " + error_string
        # if error_level == USBLCommands.ABORT_AND_SURFACE:
        elif error_level == 3:
            self.recovery_action_status = "Abort and Surface: " + error_string
        # elif error_level == USBLCommands.EMERGENCY_SURFACE:
        elif error_level == 4:
            self.recovery_action_status = "Emergency Surface: " + error_string
        else:
            self.recovery_action_status = ""

        if self.recovery_action_status != "":
            self.msglog.logMessage("", "Recovery Action:", 6)
            self.msglog.logMessage(self.recovery_action_status, "Recovery Action:", 6)

    def get_status(self):
        return self.status, self.status_color

    def get_recovery_action_status(self):
        return self.recovery_action_status

    def is_mission_in_execution(self):
        return self.mission_is_executing

    def disconnect(self):
        self.status = ""
        self.recovery_action_status = ""
        self.connected = False
        self.timer.stop()
