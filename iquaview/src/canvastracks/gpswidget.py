"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to connect to the GPS and show its track on the map.
"""

import math
import logging
from importlib import util

from PyQt5.QtWidgets import QWidget, QMessageBox
from PyQt5.QtCore import QTimer, Qt, pyqtSignal
from PyQt5.QtGui import QColor

from qgis.core import QgsPointXY, QgsWkbTypes, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsProject

from iquaview.src.ui.ui_gps import Ui_GPSwidget
from iquaview.src.canvastracks.canvasmarker import CanvasMarker

from iquaview_lib.cola2api.gps_driver import GPSDriver, GPSPosition, GPSOrientation, ConfigGPSDriver
from iquaview_lib.utils import calcutils

logger = logging.getLogger(__name__)


class GPSWidget(QWidget, Ui_GPSwidget):
    position_orientation_signal = pyqtSignal(GPSPosition, GPSOrientation)

    def __init__(self, canvas, config, parent=None):
        """
        Init of the object GPSWidget

        :param canvas: canvas to update markers and tracks into
        :type canvas: QgsMapCanvas
        :param config: config to read values from
        :type config: Config
        """

        super(GPSWidget, self).__init__(parent)
        self.setupUi(self)

        self.canvas = canvas
        self.config = config
        self.default_color = QColor(Qt.darkGreen)
        vessel_name = self.config.csettings['vessel_name']
        width = self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"]
        length = self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"]

        self.marker = CanvasMarker(self.canvas, self.default_color,
                                   ":/resources/vessel.svg", width, length,
                                   marker_mode=True, config=config)
        self.trackwidget.init("GPS track",
                              self.canvas,
                              self.default_color,
                              QgsWkbTypes.LineGeometry,
                              self.marker,
                              has_connection_lost_marker=True)

        self.gps = None
        self.connected = False
        self.set_label_disconnected()

        # set signals
        self.connectButton.clicked.connect(self.connect)

        self.timer = QTimer()
        self.timer.timeout.connect(self.gps_update_canvas)

    def connect(self):
        """ Connects to the gps if not already connected, else disconnects. """
        if not self.connected:
            try:
                if self.config.csettings['gps_configure_heading']:
                    if self.config.csettings['gps_heading_serial']:
                        if self.config.csettings['gps_serial']:
                            config = ConfigGPSDriver()
                            config.gga_serial_port = self.config.csettings['gps_serial_port']
                            config.gga_baud_rate = self.config.csettings['gps_serial_baudrate']
                            config.hdt_serial_port = self.config.csettings['gps_heading_serial_port']
                            config.hdt_baud_rate =  self.config.csettings['gps_heading_serial_baudrate']
                        else:
                            config = ConfigGPSDriver()
                            config.gga_ip = self.config.csettings['gps_ip']
                            config.gga_port = self.config.csettings['gps_gga_port']
                            config.gga_protocol = self.config.csettings['gps_protocol']
                            config.hdt_serial_port = self.config.csettings['gps_heading_serial_port']
                            config.hdt_baud_rate = self.config.csettings['gps_heading_serial_baudrate']
                    else:
                        if self.config.csettings['gps_serial']:
                            config = ConfigGPSDriver()
                            config.gga_serial_port = self.config.csettings['gps_serial_port']
                            config.gga_baud_rate = self.config.csettings['gps_serial_baudrate']
                            config.hdt_ip = self.config.csettings['gps_heading_ip']
                            config.hdt_port = self.config.csettings['gps_hdt_port']
                            config.hdt_protocol = self.config.csettings['gps_heading_protocol']
                        else:
                            config = ConfigGPSDriver()
                            config.gga_ip = self.config.csettings['gps_ip']
                            config.gga_port = self.config.csettings['gps_gga_port']
                            config.gga_protocol = self.config.csettings['gps_protocol']
                            config.hdt_ip = self.config.csettings['gps_heading_ip']
                            config.hdt_port = self.config.csettings['gps_hdt_port']
                            config.hdt_protocol = self.config.csettings['gps_heading_protocol']

                elif (util.find_spec('iquaview_evologics_usbl') is not None
                      and self.config.csettings['usbl_imu_heading_enable']):
                    if self.config.csettings['gps_serial']:
                        config = ConfigGPSDriver()
                        config.gga_serial_port = self.config.csettings['gps_serial_port']
                        config.gga_baud_rate = self.config.csettings['gps_serial_baudrate']

                    else:
                        config = ConfigGPSDriver()
                        config.gga_ip = self.config.csettings['gps_ip']
                        config.gga_port = self.config.csettings['gps_gga_port']
                        config.gga_protocol = self.config.csettings['gps_protocol']

                    config.hdt_ip = self.config.csettings['usbl_ip']
                    config.hdt_port = self.config.csettings['usbl_imu_port']
                    config.hdt_protocol = "TCP"

                elif self.config.csettings['gps_serial']:
                    config = ConfigGPSDriver()
                    config.gga_serial_port = self.config.csettings['gps_serial_port']
                    config.gga_baud_rate = self.config.csettings['gps_serial_baudrate']
                    config.hdt_serial_port = self.config.csettings['gps_serial_port']
                    config.hdt_baud_rate = self.config.csettings['gps_serial_baudrate']
                else:
                    config = ConfigGPSDriver()
                    config.gga_ip = self.config.csettings['gps_ip']
                    config.gga_port = self.config.csettings['gps_gga_port']
                    config.gga_protocol = self.config.csettings['gps_protocol']
                    config.hdt_ip = self.config.csettings['gps_ip']
                    config.hdt_port = self.config.csettings['gps_gga_port']
                    config.hdt_protocol = self.config.csettings['gps_protocol']

                # offset
                vessel_name = self.config.csettings['vessel_name']
                config.offset_yaw_deg = self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_heading']

                self.gps = GPSDriver(config)
                self.gps.connect()

                self.gps.gpsconnectionfailed.connect(self.connection_failed)
                self.gps.gpsparsingfailed.connect(self.parsing_gga_failed)
                self.connectButton.setText("Disconnect")
                self.connected = True
                self.timer.start(1000)
                self.gps_status_label.setText("Connected")
                self.gps_status_label.setStyleSheet('font:italic; color:green')
                if self.trackwidget.connection_lost_marker.isVisible():
                    self.trackwidget.connection_lost_marker.hide()
            except Exception as e:
                logger.error("Connection with GPS could not be established. {}".format(e))
                QMessageBox.critical(self,
                                     "Connection Failed",
                                     "Connection with GPS could not be established",
                                     QMessageBox.Close)
                self.connected = False
                self.set_label_disconnected()
                self.disconnect()
        else:
            self.disconnect()

    def disconnect(self):
        """ Disconnects the gps """
        if self.gps is not None:
            self.gps.close()
        self.timer.stop()
        self.connected = False
        self.connectButton.setText("Connect")
        if not self.trackwidget.connection_lost_marker.isVisible():
            self.trackwidget.connection_lost_marker.show()

        self.set_label_disconnected()

    def clear_tracks(self):
        """ Clears the gps track and marker """
        self.trackwidget.close()

    def gps_update_canvas(self):
        """ Updates new values from the gps into the trackwidget that draws over the canvas. """
        if self.connected:
            gps_position: GPSPosition = self.gps.get_position()
            gps_orientation: GPSOrientation = self.gps.get_orientation()
            self.position_orientation_signal.emit(gps_position, gps_orientation)
            if gps_position.is_new() and (gps_position.quality >= 1) and (gps_position.quality <= 5):
                self.gps_status_label.setText("Connected, receiving signal")
                self.gps_status_label.setStyleSheet('font:italic; color:green')

                gps_lat = gps_position.latitude
                gps_lon = gps_position.longitude
                if gps_orientation.is_new():
                    gps_heading = gps_orientation.orientation_deg
                else:
                    gps_heading = 0
                    self.parsing_hdt_failed()

                vessel_name = self.config.csettings['vessel_name']
                brng = (math.radians(gps_heading)
                        + math.atan2(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                         + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                                     self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                         + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x']))
                distance = - math.sqrt(
                    math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                 + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x'],
                             2)
                    + math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                   + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                               2))  # Distance in m

                gps_lon, gps_lat = calcutils.endpoint_sphere(gps_lon, gps_lat, distance,
                                                             math.degrees(brng))

                gps_crs = QgsCoordinateReferenceSystem("EPSG:4326")
                if gps_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
                    trans = QgsCoordinateTransform(gps_crs,
                                                   self.canvas.mapSettings().destinationCrs(),
                                                   QgsProject.instance().transformContext())
                    pos = trans.transform(gps_lon, gps_lat)
                else:
                    pos = QgsPointXY(gps_lon, gps_lat)

                self.trackwidget.track_update_canvas(pos, math.radians(gps_heading))

                # Show gps quality
                self.gps_fix_quality_label.setText(gps_position.get_fix_quality_as_string())

            elif not gps_position.is_new():
                self.parsing_gga_failed()

    def set_label_disconnected(self):
        """ Sets the status label to 'Disconnected' """
        self.gps_status_label.setText("Disconnected")
        self.gps_status_label.setStyleSheet('font:italic; color:red')
        self.gps_fix_quality_label.setText("")

    def parsing_gga_failed(self):
        """ Sets the status label to 'Connected, NO signal' """
        self.gps_status_label.setText("Connected, NO signal")
        self.gps_status_label.setStyleSheet('font:italic; color:red')

    def parsing_hdt_failed(self):
        """ Sets the status label to 'Connected, NO orientation' """
        self.gps_status_label.setText("Connected, NO orientation")
        self.gps_status_label.setStyleSheet('font:italic; color:red')

    def connection_failed(self):
        """ Shows a messagebox showing a connection error. """
        QMessageBox.critical(self,
                             "Connection Failed",
                             "Connection with GPS could not be established",
                             QMessageBox.Close)
        self.disconnect()

    def is_connected(self):
        """
        Returns if the gps is connected
        :return: connected status
        :rtype: bool
        """
        return self.connected

    def update_width_and_length(self):
        """ Updates width and length of the marker from the data in config. """
        vessel_name = self.config.csettings['vessel_name']

        width = self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"]
        length = self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"]
        self.marker.set_width(width)
        self.marker.set_length(length)
        self.gps_update_canvas()

    def get_status_label_text(self):
        """
        Returns gps status label text
        :return: label text
        :rtype: str
        """
        return self.gps_status_label.text()

    def get_track_widget(self):
        """
        Returns the trackwidget object
        :return: track widget
        :rtype: TrackWidget
        """
        return self.trackwidget

    def get_default_color(self):
        """
        Returns the default color of the marker
        :return: default color
        :rtype: QColor
        """
        return self.default_color

    def get_gps_data(self):
        """
        Return gps position and gps heading
        :return: return position and heading
        :rtype: [GPSPosition, GPSOrientation]
        """
        return self.gps.get_position(), self.gps.get_orientation()
