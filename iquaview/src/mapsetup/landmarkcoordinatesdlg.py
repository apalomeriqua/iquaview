"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to show the coordinates of a landmark point
"""

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QApplication

from iquaview.src.ui.ui_lm_coords_dlg import Ui_LandmarkCoordinatesDialog


class LandmarkCoordinatesDlg(QDialog, Ui_LandmarkCoordinatesDialog):
    def __init__(self, lat, lon, parent=None):
        super(LandmarkCoordinatesDlg, self).__init__(parent)
        self.setupUi(self)

        self.latitude_lineEdit.setText(str(lat))
        self.longitude_lineEdit.setText(str(lon))

        self.copy_button.setIcon(QIcon(":/resources/mActionCopyClipboard.svg"))
        self.copy_button.setToolTip("Copy to clipboard")
        self.copy_button.clicked.connect(self.copy_to_clipboard)

    def copy_to_clipboard(self):
        """Copy coordinates to clipboard"""
        cb = QApplication.clipboard()
        cb.clear()
        cb.setText("{}, {}".format(self.latitude_lineEdit.text(), self.longitude_lineEdit.text()))
