"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to add and landmark point (latitude, longitude)
"""

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox, QSizePolicy
from PyQt5.QtGui import QIcon

from qgis.core import (QgsFeature,
                       QgsVectorLayer,
                       QgsPointXY,
                       QgsGeometry,
                       QgsMapLayer,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransform,
                       QgsProject)

from iquaview.src.ui.ui_point_feature import Ui_PointFeature
from iquaview.src.tools import getpointtool
from iquaview.src.tools.coordinateconverterwidget import CoordinateConverterLineEdit


class PointFeatureDlg(QDialog, Ui_PointFeature):
    map_tool_change_signal = pyqtSignal()
    landmark_added = pyqtSignal(QgsMapLayer)
    landmark_removed = pyqtSignal(QgsMapLayer)
    finish_add_landmark_signal = pyqtSignal()

    def __init__(self, canvas, proj, parent=None):
        """
        Init of the object PointFeatureDlg
        :param canvas: canvas where the landmark will be added
        :type canvas: QgsMapCanvas
        :param proj: project where the landmark layer will be aded
        :type proj: QgsProject
        """
        super(PointFeatureDlg, self).__init__(parent)
        self.setupUi(self)

        self.point = None
        self.point_layer = None
        self.feat = None
        self.num = 1
        self.canvas = canvas
        self.proj = proj

        self.getCoordinatesButton.setIcon(QIcon(":/resources/pickPointInMap.svg"))
        self.getCoordinatesButton.clicked.connect(self.set_point_tool)
        self.getCoordinatesButton.setToolTip("Pick point in map")

        self.tool_get_point = getpointtool.GetPointTool(canvas)
        self.tool_get_point.point_signal.connect(self.add_new_landmark)

        self.coordinate_converter_lineedit = CoordinateConverterLineEdit()

        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.coordinate_converter_lineedit.sizePolicy().hasHeightForWidth())
        self.coordinate_converter_lineedit.setSizePolicy(sizePolicy)
        self.coordinates_vboxlayout.addWidget(self.coordinate_converter_lineedit)

        self.coordinate_converter_lineedit.converted_signal.connect(self.set_coordinates)

    def set_coordinates(self):
        """ Set point coordinates from coordinate converter lineedit"""
        if (self.coordinate_converter_lineedit.get_longitude() is not None
                and self.coordinate_converter_lineedit.get_latitude() is not None):
            self.point = QgsPointXY(float(self.coordinate_converter_lineedit.get_longitude()),
                                    float(self.coordinate_converter_lineedit.get_latitude()))
        else:
            self.point = None

    def get_coordinates(self):
        """ Return the coordinates (latitude, longitude)
        :return : latitude, longitude
        :rtype: float, float
        """
        return self.point.y(), self.point.x()

    def set_point_tool(self):
        """Set the maptool to pointTool"""
        self.map_tool_change_signal.emit()  # Emit signal to warn mainwindow that we are changing a maptool
        self.canvas.setMapTool(self.tool_get_point)

    def add_new_landmark(self, point, default_crs=False):
        """
        create a landmark point
        :param point: Point to add on the canvas
        :type point: QgsPointXY
        :param default_crs: bool to use EPSG:4326 as default crs
        :type default_crs: bool
        """
        if default_crs:
            origin_crs = QgsCoordinateReferenceSystem("EPSG:4326")
        else:
            origin_crs = self.canvas.mapSettings().destinationCrs()

        if self.feat is not None:
            self.point_layer.dataProvider().deleteFeatures([self.feat.id()])
            self.landmark_removed.emit(self.point_layer)

        landmarks_crs = QgsCoordinateReferenceSystem("EPSG:4326")
        if landmarks_crs.authid() != origin_crs.authid():
            trans = QgsCoordinateTransform(origin_crs,
                                           landmarks_crs,
                                           QgsProject.instance().transformContext())
            point = trans.transform(point.x(), point.y())

        self.point = point
        name = "LandmarkPoint_" + str(self.num)
        restart_search = True
        # This loops tries to find a free name for the LandmarkPoint_
        while restart_search:
            restart_search = False
            # for every layer
            for layer in self.proj.mapLayers().values():
                # check name
                if layer.name() == name:
                    self.num += 1
                    name = "LandmarkPoint_" + str(self.num)
                    # Set to True to find the next LandmarkPoint
                    restart_search = True

        self.point_layer = QgsVectorLayer(
            "Point?crs=epsg:4326",
            name,
            "memory")

        self.feat = QgsFeature()
        self.feat.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(self.point.x(), self.point.y())))
        self.point_layer.dataProvider().addFeatures([self.feat])
        self.landmark_added.emit(self.point_layer)

        self.coordinate_converter_lineedit.setText("{}, {}".format(self.point.y(), self.point.x()))

    def reset(self):
        """ Reset params"""
        self.point = None
        self.point_layer = None
        self.feat = None

        self.coordinate_converter_lineedit.setText("")

    def accept(self):
        """ Insert self.point in the canvas"""
        if self.point is not None:
            coord = self.get_coordinates()
            new_point = QgsPointXY(coord[1], coord[0])
            if self.point_layer is None:
                self.add_new_landmark(self.point, True)

            self.point_layer.startEditing()
            self.point_layer.beginEditCommand("Move Point")  # for undo
            self.point_layer.moveVertex(new_point.x(), new_point.y(), self.feat.id() + 1, 0)
            self.point_layer.endEditCommand()
            self.point_layer.commitChanges()

            self.finish_add_landmark_signal.emit()
            super(PointFeatureDlg, self).accept()
            self.reset()
        else:
            QMessageBox.warning(self,
                                "Invalid Point",
                                "Invalid point, make sure the coordinates are correct.",
                                QMessageBox.Close)

    def reject(self):
        """ reject point insertion"""
        if self.feat is not None:
            # if we are aborting, point is deleted
            self.point_layer.dataProvider().deleteFeatures([self.feat.id()])
            self.landmark_removed.emit(self.point_layer)

        self.finish_add_landmark_signal.emit()
        super(PointFeatureDlg, self).reject()

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout to be deleted
        :type layout: QHBoxLayout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())
