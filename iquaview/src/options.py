"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to configure app.config parameters.
 It includes last_auv_config_xml, remote_vehicle_package, user, coordinate display.
"""

import logging

from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, QSize

from iquaview.src.ui.ui_options import Ui_OptionsDlg
from iquaview.src.generaloptions import GeneralOptionsWidget
from iquaview.src.styles import StylesWidget
from iquaview.src.tools.vesselpossystem import VesselPositionSystem
from iquaview.src.connection.settings.connectionsettings import ConnectionSettingsWidget
from iquaview.src.customizationsettings import CustomizationSettings
from iquaview.src.connection.dataoutputmanager import DataOutputManager
from iquaview.src.tools.web_server.webserver import WebServer
logger = logging.getLogger(__name__)

GENERAL = 0
CONNECTION_SETTINGS = 1
CUSTOMIZATION_SETTINGS = 2
DATA_OUTPUT_CONNECTION = 3
STYLES = 4
VESSEL_POSITION_SYSTEM = 5
WEB_SERVER = 6

LIST_SPACING = 3
ICON_SIZE = 25


class OptionsDlg(QDialog, Ui_OptionsDlg):
    def __init__(self, config, vehicle_info, vehicle_data, canvas, mission_ctrl, north_arrow, scale_bar, usbl_module=None, parent=None):
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info:  VehicleInfo
        :param vehicledata: data received from the AUV
        :type vehicledata: VehicleData
        :param canvas: canvas where repaint events will be triggered
        :type QgsMapCanvas
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        :param north_arrow: Canvas item to change color
        :type north_arrow: NorthArrow
        :param scale_bar: Canvas item to change color
        :type scale_bar: ScaleBar
        """
        super(OptionsDlg, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Options")
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.canvas = canvas
        self.mission_ctrl = mission_ctrl
        self.north_arrow = north_arrow
        self.scale_bar = scale_bar
        self.usbl_module = usbl_module

        #icons
        general_icon = QIcon(":/resources/mActionOptions.svg")
        connection_icon = QIcon(":/resources/mActionConnectionSettings.svg")
        customization_icon = QIcon(":/resources/customizationSettings.svg")
        styles_icon = QIcon(":/resources/mActionStyles.svg")
        vessel_icon = QIcon(":/resources/mActionVesselPosSystem.svg")
        data_output_icon = QIcon(":/resources/uploadPosition.svg")
        web_server_icon = QIcon(":/resources/web_server_icon.svg")


        # set icons
        self.mOptionsListWidget.item(GENERAL).setIcon(general_icon)
        self.mOptionsListWidget.item(CONNECTION_SETTINGS).setIcon(connection_icon)
        self.mOptionsListWidget.item(CUSTOMIZATION_SETTINGS).setIcon(customization_icon)
        self.mOptionsListWidget.item(STYLES).setIcon(styles_icon)
        self.mOptionsListWidget.item(VESSEL_POSITION_SYSTEM).setIcon(vessel_icon)
        self.mOptionsListWidget.item(DATA_OUTPUT_CONNECTION).setIcon(data_output_icon)
        self.mOptionsListWidget.item(WEB_SERVER).setIcon(web_server_icon)

        self.mOptionsListWidget.setIconSize(QSize(ICON_SIZE, ICON_SIZE))
        self.mOptionsListWidget.setSpacing(LIST_SPACING)

        self.general_options_widget = GeneralOptionsWidget(self.config, self.vehicle_info, self)
        self.connection_settings_widget = ConnectionSettingsWidget(self.config, self.vehicle_info, self)
        self.customization_settings_widget = CustomizationSettings(self.config, self.vehicle_info)
        self.styles_widget = StylesWidget(self.config, self.canvas, self.mission_ctrl, self.north_arrow,
                                          self.scale_bar)
        self.vessel_pos_system_widget = VesselPositionSystem(self.config, parent=None)
        self.data_output_manager_widget = DataOutputManager(self.config, self.vehicle_data, self.usbl_module)
        self.web_server_widget = WebServer(self.config, self.vehicle_data, self.vehicle_info)

        self.mOptionsStackedWidget.addWidget(self.general_options_widget)
        self.mOptionsStackedWidget.addWidget(self.connection_settings_widget)
        self.mOptionsStackedWidget.addWidget(self.customization_settings_widget)
        self.mOptionsStackedWidget.addWidget(self.data_output_manager_widget)
        self.mOptionsStackedWidget.addWidget(self.styles_widget)
        self.mOptionsStackedWidget.addWidget(self.vessel_pos_system_widget)
        self.mOptionsStackedWidget.addWidget(self.web_server_widget)
        self.update_options(force_update=True)

        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)

        self.applyButton.clicked.connect(self.on_apply)
        self.closeButton.clicked.connect(self.on_cancel)
        self.saveButton.clicked.connect(self.on_accept)

    def set_current_page(self, item):
        """
        Change item in Stacked widget
        :param item: position of the page
        :type item: int
        """
        self.mOptionsListWidget.currentRowChanged.disconnect(self.set_current_page)
        self.mOptionsStackedWidget.setCurrentIndex(item)
        self.mOptionsListWidget.setCurrentRow(item)
        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)
        self.update_options()

    def update_options(self, force_update=False):
        """Update and reload menu options"""
        self.applyButton.setEnabled(True)
        if self.mOptionsStackedWidget.currentIndex() == GENERAL or force_update:
            self.general_options_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == CONNECTION_SETTINGS or force_update:
            self.connection_settings_widget.load_settings()

        if self.mOptionsStackedWidget.currentIndex() == CUSTOMIZATION_SETTINGS or force_update:
            self.applyButton.setEnabled(False)
            self.customization_settings_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == STYLES or force_update:
            self.styles_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == VESSEL_POSITION_SYSTEM or force_update:
            self.vessel_pos_system_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION or force_update:
            self.data_output_manager_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == WEB_SERVER or force_update:
            self.web_server_widget.reload()

    def keyPressEvent(self, e) -> None:
        """ Override event handler keyPressEvent"""
        if e.key() == Qt.Key_Escape:
            self.on_cancel()
            super(OptionsDlg, self).keyPressEvent(e)

    def closeEvent(self, event):
        """ Override of the closeEvent methond. Calls on_cancel to restore changed colors """
        self.on_cancel()

    def on_apply(self):

        if self.mOptionsStackedWidget.currentIndex() == GENERAL:
            self.general_options_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == CONNECTION_SETTINGS:
            self.connection_settings_widget.apply_settings()

        elif self.mOptionsStackedWidget.currentIndex() == STYLES:
            self.styles_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == VESSEL_POSITION_SYSTEM:
            self.vessel_pos_system_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION:
            self.data_output_manager_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == WEB_SERVER:
            self.web_server_widget.on_apply()

    def on_accept(self):
        """On click 'Save as Defaults' accept"""
        accept = True        
        if self.mOptionsStackedWidget.currentIndex() == GENERAL:
            accept = self.general_options_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == CONNECTION_SETTINGS:
            accept = self.connection_settings_widget.save()

        elif self.mOptionsStackedWidget.currentIndex() == CUSTOMIZATION_SETTINGS:
            self.customization_settings_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == STYLES:
            accept = self.styles_widget.save_as_defaults()

        elif self.mOptionsStackedWidget.currentIndex() == VESSEL_POSITION_SYSTEM:
            accept = self.vessel_pos_system_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION:
            accept = self.data_output_manager_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == WEB_SERVER:
            self.web_server_widget.on_accept()
        if accept:
            self.accept()

    def on_cancel(self):

        if self.mOptionsStackedWidget.currentIndex() == STYLES:
            self.styles_widget.on_cancel()

        self.reject()
