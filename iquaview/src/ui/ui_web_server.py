# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_web_server.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WebServerWidget(object):
    def setupUi(self, WebServerWidget):
        WebServerWidget.setObjectName("WebServerWidget")
        WebServerWidget.resize(331, 80)
        self.gridLayout = QtWidgets.QGridLayout(WebServerWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.state_label = QtWidgets.QLabel(WebServerWidget)
        self.state_label.setObjectName("state_label")
        self.gridLayout.addWidget(self.state_label, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 2, 1, 1, 1)
        self.port_label = QtWidgets.QLabel(WebServerWidget)
        self.port_label.setObjectName("port_label")
        self.gridLayout.addWidget(self.port_label, 0, 2, 1, 1, QtCore.Qt.AlignRight)
        self.state_hboxlayout = QtWidgets.QHBoxLayout()
        self.state_hboxlayout.setObjectName("state_hboxlayout")
        self.gridLayout.addLayout(self.state_hboxlayout, 0, 1, 1, 1)
        self.port_text = QtWidgets.QLineEdit(WebServerWidget)
        self.port_text.setEnabled(True)
        self.port_text.setMaximumSize(QtCore.QSize(55, 16777215))
        self.port_text.setObjectName("port_text")
        self.gridLayout.addWidget(self.port_text, 0, 3, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 0, 4, 1, 1)

        self.retranslateUi(WebServerWidget)
        QtCore.QMetaObject.connectSlotsByName(WebServerWidget)

    def retranslateUi(self, WebServerWidget):
        _translate = QtCore.QCoreApplication.translate
        WebServerWidget.setWindowTitle(_translate("WebServerWidget", "Form"))
        self.state_label.setText(_translate("WebServerWidget", "Web Server State:"))
        self.port_label.setText(_translate("WebServerWidget", "Port:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WebServerWidget = QtWidgets.QWidget()
    ui = Ui_WebServerWidget()
    ui.setupUi(WebServerWidget)
    WebServerWidget.show()
    sys.exit(app.exec_())

