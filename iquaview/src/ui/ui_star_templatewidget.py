# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_star_templatewidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_StarTemplateWidget(object):
    def setupUi(self, StarTemplateWidget):
        StarTemplateWidget.setObjectName("StarTemplateWidget")
        StarTemplateWidget.resize(304, 590)
        self.verticalLayout = QtWidgets.QVBoxLayout(StarTemplateWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.missionAreaGroupBox = QtWidgets.QGroupBox(StarTemplateWidget)
        self.missionAreaGroupBox.setMinimumSize(QtCore.QSize(0, 245))
        self.missionAreaGroupBox.setMaximumSize(QtCore.QSize(16777215, 245))
        self.missionAreaGroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.missionAreaGroupBox.setObjectName("missionAreaGroupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.missionAreaGroupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.alongTLength = QtWidgets.QDoubleSpinBox(self.missionAreaGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alongTLength.sizePolicy().hasHeightForWidth())
        self.alongTLength.setSizePolicy(sizePolicy)
        self.alongTLength.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.alongTLength.setMaximum(99999.99)
        self.alongTLength.setObjectName("alongTLength")
        self.gridLayout.addWidget(self.alongTLength, 2, 1, 1, 1)
        self.alongTrackLabel = QtWidgets.QLabel(self.missionAreaGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alongTrackLabel.sizePolicy().hasHeightForWidth())
        self.alongTrackLabel.setSizePolicy(sizePolicy)
        self.alongTrackLabel.setObjectName("alongTrackLabel")
        self.gridLayout.addWidget(self.alongTrackLabel, 2, 0, 1, 1)
        self.acrossTrackLabel = QtWidgets.QLabel(self.missionAreaGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.acrossTrackLabel.sizePolicy().hasHeightForWidth())
        self.acrossTrackLabel.setSizePolicy(sizePolicy)
        self.acrossTrackLabel.setObjectName("acrossTrackLabel")
        self.gridLayout.addWidget(self.acrossTrackLabel, 3, 0, 1, 1)
        self.acrossTLength = QtWidgets.QDoubleSpinBox(self.missionAreaGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.acrossTLength.sizePolicy().hasHeightForWidth())
        self.acrossTLength.setSizePolicy(sizePolicy)
        self.acrossTLength.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.acrossTLength.setMaximum(99999.99)
        self.acrossTLength.setObjectName("acrossTLength")
        self.gridLayout.addWidget(self.acrossTLength, 3, 1, 1, 1)
        self.automaticExtent = QtWidgets.QRadioButton(self.missionAreaGroupBox)
        self.automaticExtent.setChecked(True)
        self.automaticExtent.setObjectName("automaticExtent")
        self.gridLayout.addWidget(self.automaticExtent, 0, 0, 1, 2)
        self.fixedExtent = QtWidgets.QRadioButton(self.missionAreaGroupBox)
        self.fixedExtent.setObjectName("fixedExtent")
        self.gridLayout.addWidget(self.fixedExtent, 1, 0, 1, 2)
        self.centerOnTargetButton = QtWidgets.QPushButton(self.missionAreaGroupBox)
        self.centerOnTargetButton.setObjectName("centerOnTargetButton")
        self.gridLayout.addWidget(self.centerOnTargetButton, 5, 0, 1, 2)
        self.drawRectangleButton = QtWidgets.QPushButton(self.missionAreaGroupBox)
        self.drawRectangleButton.setObjectName("drawRectangleButton")
        self.gridLayout.addWidget(self.drawRectangleButton, 6, 0, 1, 2)
        self.verticalLayout.addWidget(self.missionAreaGroupBox)
        self.InitialPointgroupBox = QtWidgets.QGroupBox(StarTemplateWidget)
        self.InitialPointgroupBox.setMinimumSize(QtCore.QSize(0, 85))
        self.InitialPointgroupBox.setMaximumSize(QtCore.QSize(16777215, 75))
        self.InitialPointgroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.InitialPointgroupBox.setObjectName("InitialPointgroupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.InitialPointgroupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.initial_point_label = QtWidgets.QLabel(self.InitialPointgroupBox)
        self.initial_point_label.setObjectName("initial_point_label")
        self.gridLayout_2.addWidget(self.initial_point_label, 1, 0, 1, 1)
        self.number_points_label = QtWidgets.QLabel(self.InitialPointgroupBox)
        self.number_points_label.setObjectName("number_points_label")
        self.gridLayout_2.addWidget(self.number_points_label, 0, 0, 1, 1)
        self.number_points_spinBox = QtWidgets.QSpinBox(self.InitialPointgroupBox)
        self.number_points_spinBox.setMinimum(3)
        self.number_points_spinBox.setMaximum(200)
        self.number_points_spinBox.setProperty("value", 3)
        self.number_points_spinBox.setObjectName("number_points_spinBox")
        self.gridLayout_2.addWidget(self.number_points_spinBox, 0, 1, 1, 1)
        self.initial_point_spinBox = QtWidgets.QSpinBox(self.InitialPointgroupBox)
        self.initial_point_spinBox.setWrapping(True)
        self.initial_point_spinBox.setMinimum(1)
        self.initial_point_spinBox.setMaximum(200)
        self.initial_point_spinBox.setProperty("value", 1)
        self.initial_point_spinBox.setObjectName("initial_point_spinBox")
        self.gridLayout_2.addWidget(self.initial_point_spinBox, 1, 1, 1, 1)
        self.verticalLayout.addWidget(self.InitialPointgroupBox)
        self.transectsGroupBox = QtWidgets.QGroupBox(StarTemplateWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.transectsGroupBox.sizePolicy().hasHeightForWidth())
        self.transectsGroupBox.setSizePolicy(sizePolicy)
        self.transectsGroupBox.setMaximumSize(QtCore.QSize(16777215, 390))
        self.transectsGroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.transectsGroupBox.setObjectName("transectsGroupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.transectsGroupBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.heave_mode_widget = HeaveModeWidget(self.transectsGroupBox)
        self.heave_mode_widget.setObjectName("heave_mode_widget")
        self.verticalLayout_2.addWidget(self.heave_mode_widget)
        self.line = QtWidgets.QFrame(self.transectsGroupBox)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.no_altitude_horizontalLayout = QtWidgets.QHBoxLayout()
        self.no_altitude_horizontalLayout.setObjectName("no_altitude_horizontalLayout")
        self.no_altitude_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.no_altitude_label.setObjectName("no_altitude_label")
        self.no_altitude_horizontalLayout.addWidget(self.no_altitude_label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.no_altitude_horizontalLayout.addItem(spacerItem)
        self.no_altitude_comboBox = QtWidgets.QComboBox(self.transectsGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_altitude_comboBox.sizePolicy().hasHeightForWidth())
        self.no_altitude_comboBox.setSizePolicy(sizePolicy)
        self.no_altitude_comboBox.setObjectName("no_altitude_comboBox")
        self.no_altitude_comboBox.addItem("")
        self.no_altitude_comboBox.addItem("")
        self.no_altitude_horizontalLayout.addWidget(self.no_altitude_comboBox)
        self.verticalLayout_2.addLayout(self.no_altitude_horizontalLayout)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.speed_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.speed_label.setObjectName("speed_label")
        self.horizontalLayout_4.addWidget(self.speed_label)
        self.speed_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.transectsGroupBox)
        self.speed_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.speed_doubleSpinBox.setSingleStep(0.1)
        self.speed_doubleSpinBox.setProperty("value", 0.5)
        self.speed_doubleSpinBox.setObjectName("speed_doubleSpinBox")
        self.horizontalLayout_4.addWidget(self.speed_doubleSpinBox)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.tolerance_horizontalLayout = QtWidgets.QHBoxLayout()
        self.tolerance_horizontalLayout.setObjectName("tolerance_horizontalLayout")
        self.tolerance_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.tolerance_label.setObjectName("tolerance_label")
        self.tolerance_horizontalLayout.addWidget(self.tolerance_label)
        self.xy_tolerance_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.transectsGroupBox)
        self.xy_tolerance_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.xy_tolerance_doubleSpinBox.setMaximum(100000.0)
        self.xy_tolerance_doubleSpinBox.setSingleStep(0.5)
        self.xy_tolerance_doubleSpinBox.setProperty("value", 2.0)
        self.xy_tolerance_doubleSpinBox.setObjectName("xy_tolerance_doubleSpinBox")
        self.tolerance_horizontalLayout.addWidget(self.xy_tolerance_doubleSpinBox)
        self.verticalLayout_2.addLayout(self.tolerance_horizontalLayout)
        self.verticalLayout.addWidget(self.transectsGroupBox)

        self.retranslateUi(StarTemplateWidget)
        QtCore.QMetaObject.connectSlotsByName(StarTemplateWidget)
        StarTemplateWidget.setTabOrder(self.automaticExtent, self.fixedExtent)
        StarTemplateWidget.setTabOrder(self.fixedExtent, self.alongTLength)
        StarTemplateWidget.setTabOrder(self.alongTLength, self.acrossTLength)
        StarTemplateWidget.setTabOrder(self.acrossTLength, self.centerOnTargetButton)
        StarTemplateWidget.setTabOrder(self.centerOnTargetButton, self.drawRectangleButton)
        StarTemplateWidget.setTabOrder(self.drawRectangleButton, self.number_points_spinBox)
        StarTemplateWidget.setTabOrder(self.number_points_spinBox, self.initial_point_spinBox)
        StarTemplateWidget.setTabOrder(self.initial_point_spinBox, self.no_altitude_comboBox)
        StarTemplateWidget.setTabOrder(self.no_altitude_comboBox, self.speed_doubleSpinBox)
        StarTemplateWidget.setTabOrder(self.speed_doubleSpinBox, self.xy_tolerance_doubleSpinBox)

    def retranslateUi(self, StarTemplateWidget):
        _translate = QtCore.QCoreApplication.translate
        StarTemplateWidget.setWindowTitle(_translate("StarTemplateWidget", "Form"))
        self.missionAreaGroupBox.setTitle(_translate("StarTemplateWidget", "Mission Area"))
        self.alongTrackLabel.setText(_translate("StarTemplateWidget", "Height"))
        self.acrossTrackLabel.setText(_translate("StarTemplateWidget", "Width"))
        self.automaticExtent.setText(_translate("StarTemplateWidget", "Automatic extent"))
        self.fixedExtent.setText(_translate("StarTemplateWidget", "Fixed extent "))
        self.centerOnTargetButton.setText(_translate("StarTemplateWidget", "Center on Target"))
        self.drawRectangleButton.setText(_translate("StarTemplateWidget", "Draw Rectangle"))
        self.InitialPointgroupBox.setTitle(_translate("StarTemplateWidget", "Define Polygon"))
        self.initial_point_label.setText(_translate("StarTemplateWidget", "Initial Point:"))
        self.number_points_label.setText(_translate("StarTemplateWidget", "Number of directions:"))
        self.transectsGroupBox.setTitle(_translate("StarTemplateWidget", "Transects"))
        self.no_altitude_label.setText(_translate("StarTemplateWidget", "No Altitude:"))
        self.no_altitude_comboBox.setItemText(0, _translate("StarTemplateWidget", "Go up"))
        self.no_altitude_comboBox.setItemText(1, _translate("StarTemplateWidget", "Ignore"))
        self.speed_label.setText(_translate("StarTemplateWidget", "Speed:"))
        self.tolerance_label.setText(_translate("StarTemplateWidget", "Tolerance XY (m):"))

from iquaview.src.mission.heavemodewidget import HeaveModeWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    StarTemplateWidget = QtWidgets.QWidget()
    ui = Ui_StarTemplateWidget()
    ui.setupUi(StarTemplateWidget)
    StarTemplateWidget.show()
    sys.exit(app.exec_())

