# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_go_to_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GoToDialog(object):
    def setupUi(self, GoToDialog):
        GoToDialog.setObjectName("GoToDialog")
        GoToDialog.resize(349, 216)
        self.gridLayout = QtWidgets.QGridLayout(GoToDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.cancel_pushButton = QtWidgets.QPushButton(GoToDialog)
        self.cancel_pushButton.setObjectName("cancel_pushButton")
        self.horizontalLayout.addWidget(self.cancel_pushButton)
        self.Ok_pushButton = QtWidgets.QPushButton(GoToDialog)
        self.Ok_pushButton.setObjectName("Ok_pushButton")
        self.horizontalLayout.addWidget(self.Ok_pushButton)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.getCoordinatesButton = QtWidgets.QPushButton(GoToDialog)
        self.getCoordinatesButton.setMaximumSize(QtCore.QSize(27, 16777215))
        self.getCoordinatesButton.setText("")
        self.getCoordinatesButton.setObjectName("getCoordinatesButton")
        self.gridLayout_2.addWidget(self.getCoordinatesButton, 0, 4, 2, 1)
        self.surgeLabel = QtWidgets.QLabel(GoToDialog)
        self.surgeLabel.setObjectName("surgeLabel")
        self.gridLayout_2.addWidget(self.surgeLabel, 5, 0, 1, 1)
        self.longitudeLabel = QtWidgets.QLabel(GoToDialog)
        self.longitudeLabel.setObjectName("longitudeLabel")
        self.gridLayout_2.addWidget(self.longitudeLabel, 1, 0, 1, 1)
        self.latitudeLabel = QtWidgets.QLabel(GoToDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.latitudeLabel.sizePolicy().hasHeightForWidth())
        self.latitudeLabel.setSizePolicy(sizePolicy)
        self.latitudeLabel.setMinimumSize(QtCore.QSize(119, 0))
        self.latitudeLabel.setObjectName("latitudeLabel")
        self.gridLayout_2.addWidget(self.latitudeLabel, 0, 0, 1, 1)
        self.tolerance_label = QtWidgets.QLabel(GoToDialog)
        self.tolerance_label.setObjectName("tolerance_label")
        self.gridLayout_2.addWidget(self.tolerance_label, 7, 0, 1, 1)
        self.surge_doubleSpinBox = QtWidgets.QDoubleSpinBox(GoToDialog)
        self.surge_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.surge_doubleSpinBox.setMaximum(100000.0)
        self.surge_doubleSpinBox.setProperty("value", 0.5)
        self.surge_doubleSpinBox.setObjectName("surge_doubleSpinBox")
        self.gridLayout_2.addWidget(self.surge_doubleSpinBox, 5, 1, 1, 4)
        self.tolerance_doubleSpinBox = QtWidgets.QDoubleSpinBox(GoToDialog)
        self.tolerance_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.tolerance_doubleSpinBox.setMinimum(0.0)
        self.tolerance_doubleSpinBox.setMaximum(100000.0)
        self.tolerance_doubleSpinBox.setSingleStep(0.5)
        self.tolerance_doubleSpinBox.setProperty("value", 2.0)
        self.tolerance_doubleSpinBox.setObjectName("tolerance_doubleSpinBox")
        self.gridLayout_2.addWidget(self.tolerance_doubleSpinBox, 7, 1, 1, 4)
        self.latitudeLineEdit = QtWidgets.QLineEdit(GoToDialog)
        self.latitudeLineEdit.setObjectName("latitudeLineEdit")
        self.gridLayout_2.addWidget(self.latitudeLineEdit, 0, 1, 1, 3)
        self.longitudeLineEdit = QtWidgets.QLineEdit(GoToDialog)
        self.longitudeLineEdit.setObjectName("longitudeLineEdit")
        self.gridLayout_2.addWidget(self.longitudeLineEdit, 1, 1, 1, 3)
        self.no_altitude_label = QtWidgets.QLabel(GoToDialog)
        self.no_altitude_label.setObjectName("no_altitude_label")
        self.gridLayout_2.addWidget(self.no_altitude_label, 3, 0, 1, 1)
        self.no_altitude_comboBox = QtWidgets.QComboBox(GoToDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_altitude_comboBox.sizePolicy().hasHeightForWidth())
        self.no_altitude_comboBox.setSizePolicy(sizePolicy)
        self.no_altitude_comboBox.setObjectName("no_altitude_comboBox")
        self.no_altitude_comboBox.addItem("")
        self.no_altitude_comboBox.addItem("")
        self.gridLayout_2.addWidget(self.no_altitude_comboBox, 3, 1, 1, 4)
        self.heave_mode_widget = HeaveModeWidget(GoToDialog)
        self.heave_mode_widget.setObjectName("heave_mode_widget")
        self.gridLayout_2.addWidget(self.heave_mode_widget, 2, 0, 1, 5)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 1, 0, 1, 1)

        self.retranslateUi(GoToDialog)
        QtCore.QMetaObject.connectSlotsByName(GoToDialog)
        GoToDialog.setTabOrder(self.latitudeLineEdit, self.longitudeLineEdit)
        GoToDialog.setTabOrder(self.longitudeLineEdit, self.getCoordinatesButton)
        GoToDialog.setTabOrder(self.getCoordinatesButton, self.cancel_pushButton)
        GoToDialog.setTabOrder(self.cancel_pushButton, self.Ok_pushButton)

    def retranslateUi(self, GoToDialog):
        _translate = QtCore.QCoreApplication.translate
        GoToDialog.setWindowTitle(_translate("GoToDialog", "Go To"))
        self.cancel_pushButton.setText(_translate("GoToDialog", "Cancel"))
        self.Ok_pushButton.setText(_translate("GoToDialog", "Ok"))
        self.surgeLabel.setText(_translate("GoToDialog", "Surge (m/s):"))
        self.longitudeLabel.setText(_translate("GoToDialog", "Longitude (º):"))
        self.latitudeLabel.setText(_translate("GoToDialog", "Latitude (º):"))
        self.tolerance_label.setText(_translate("GoToDialog", "Tolerance (m):"))
        self.latitudeLineEdit.setText(_translate("GoToDialog", "0.0"))
        self.longitudeLineEdit.setText(_translate("GoToDialog", "0.0"))
        self.no_altitude_label.setToolTip(_translate("GoToDialog", "No altitude reactive behaviour"))
        self.no_altitude_label.setText(_translate("GoToDialog", "No Altitude:"))
        self.no_altitude_comboBox.setToolTip(_translate("GoToDialog", "No altitude reactive behaviour"))
        self.no_altitude_comboBox.setItemText(0, _translate("GoToDialog", "Go up"))
        self.no_altitude_comboBox.setItemText(1, _translate("GoToDialog", "Ignore"))

from iquaview.src.mission.heavemodewidget import HeaveModeWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GoToDialog = QtWidgets.QDialog()
    ui = Ui_GoToDialog()
    ui.setupUi(GoToDialog)
    GoToDialog.show()
    sys.exit(app.exec_())

