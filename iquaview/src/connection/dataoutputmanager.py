"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to export auv and usbl position by serial or ethernet
"""
import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QVBoxLayout

from iquaview.src.connection.dataoutputconnection import DataOutputConnection
from iquaview.src.ui.ui_dataoutputmanager import Ui_DataOutputManagerWidget

logger = logging.getLogger(__name__)


class DataOutputManager(QWidget, Ui_DataOutputManagerWidget):
    refresh_signal = pyqtSignal()

    def __init__(self, config, vehicle_data, usbl_module=None):
        super(DataOutputManager, self).__init__()
        self.setupUi(self)
        self.config = config
        self.vehicle_data = vehicle_data
        self.usbl_module = usbl_module
        self.connections_list = set()
        self.connections_ids = set()
        self.count = 1
        self.scrollAreaWidgetContents.setLayout(QVBoxLayout())

        self.new_connection_pushButton.clicked.connect(self.new_connection)

    def new_connection(self):
        """ Add new connection"""
        while "connection_{}".format(self.count) in self.connections_ids:
            self.count += 1
        identifier = "connection_{}".format(self.count)
        self.count += 1
        self.add_connection(identifier, identifier)

    def add_connection(self, identifier, connection_name):

        connection = DataOutputConnection(identifier, connection_name, self.config, self.vehicle_data, self.usbl_module)
        connection.remove_connection_signal.connect(self.remove_connection)
        self.connections_list.add(connection)
        self.connections_ids.add(identifier)
        self.scrollAreaWidgetContents.layout().addWidget(connection)

    def remove_connection(self):
        """ Remove a connection"""
        sender = self.sender()
        sender.disconnect_widget(reconnect=False)
        self.connections_ids.remove(sender.get_identifier())
        self.connections_list.remove(sender)
        if self.config.csettings['data_output_connection'].get(sender.get_identifier()) is not None:
            del self.config.csettings['data_output_connection'][sender.get_identifier()]
        layout = self.scrollAreaWidgetContents.layout()
        for i in reversed(range(layout.count())):
            if layout.itemAt(i).widget() == sender:
                widget_to_remove = layout.takeAt(i).widget()
                # remove it from the layout list
                layout.removeWidget(widget_to_remove)
                # remove it
                widget_to_remove.setParent(None)

    def clear(self):

        self.disconnect_widget(reconnect=False)
        self.connections_ids.clear()
        self.connections_list.clear()

        layout = self.scrollAreaWidgetContents.layout()
        for i in reversed(range(layout.count())):
            widget_to_remove = layout.takeAt(i).widget()
            # remove it from the layout list
            layout.removeWidget(widget_to_remove)
            # remove it
            widget_to_remove.setParent(None)

    def reload(self):

        self.clear()
        if (self.config.csettings.get('data_output_connection')
                and type(self.config.csettings.get('data_output_connection')) == dict):
            for key, item in self.config.csettings.get('data_output_connection').items():
                if item.get('connection_name') is not None:
                    name = item['connection_name']
                else:
                    name = key
                self.add_connection(key, name)

        for connection in self.connections_list:
            connection.reload()

        self.connect()

    def connect(self):
        for connection in self.connections_list:
            connection.start_connection_thread()

    def disconnect_widget(self, reconnect=False):
        for connection in self.connections_list:
            connection.disconnect_widget(reconnect)

    def on_apply(self):
        for connection in self.connections_list:
            connection.on_apply()

        self.reload()

    def valid_connections(self):
        is_valid = True
        for connection in self.connections_list:
            if not connection.is_valid_data():
                is_valid = False
                break
        return is_valid

    def on_accept(self):
        if self.valid_connections():
            # remove previous connections
            if self.config.settings.get('data_output_connection'):
                self.config.settings['data_output_connection'] = {}
                self.config.save()
            # add new connections
            for connection in self.connections_list:
                connection.on_accept()

            self.reload()

            return True
        else:
            return False
