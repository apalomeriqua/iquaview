"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to setup the connection of a GPS device,
 either from a serial port or a TCP/IP connection.
"""

from importlib import util

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QValidator
from PyQt5.QtCore import pyqtSignal

from qgis.core import QgsGpsDetector

from iquaview.src.ui.ui_gps_connection import Ui_GPSConnectionWidget
from iquaview_lib.utils.textvalidator import (validate_ip,
                                              validate_port,
                                              get_color,
                                              get_ip_validator,
                                              get_int_validator)


class GPSConnectionWidget(QWidget, Ui_GPSConnectionWidget):
    def __init__(self, parent=None):
        super(GPSConnectionWidget, self).__init__(parent)
        self.setupUi(self)
        self.configure_heading = False
        self.imu_heading = False
        self.serial_port = None
        self.heading_serial_port = None
        self.serial_baudrate = None
        self.heading_serial_baudrate = None
        self.ip = None
        self.heading_ip = None
        self.hdt_port = None
        self.gga_port = None
        self.protocol = None
        self.heading_protocol = None

        # set validators
        self.ip_text.setValidator(get_ip_validator())
        self.heading_ip_text.setValidator(get_ip_validator())
        int_port_validator = get_int_validator(0, 65535)
        self.hdt_port_text.setValidator(int_port_validator)
        self.gga_port_text.setValidator(int_port_validator)

        # set signals
        self.updateDevicesButton.clicked.connect(self.update_devices)
        self.ip_text.textChanged.connect(self.ip_validator)
        self.gga_port_text.textChanged.connect(self.port_validator)
        self.update_devices()

        self.heading_updateDevicesButton.clicked.connect(self.heading_update_devices)
        self.heading_ip_text.textChanged.connect(self.ip_validator)
        self.hdt_port_text.textChanged.connect(self.port_validator)
        self.heading_update_devices()

        self.configure_radioButton.toggled.emit(self.configure_radioButton.isChecked())
        if util.find_spec('iquaview_evologics_usbl') is None:
            self.usbl_imu_radioButton.hide()
            self.line_2.hide()
            self.imu_widget.hide()
            if not self.configure_radioButton.isChecked():
                self.same_radioButton.setChecked(True)
            else:
                self.configure_radioButton.setChecked(True)
        else:
            self.usbl_imu_radioButton.toggled.emit(self.usbl_imu_radioButton.isChecked())

    def update_devices(self):
        """ Load available ports and update device ComboBox """
        detector = QgsGpsDetector('scan')
        self.deviceComboBox.clear()
        devices_list = []
        for port in detector.availablePorts():
            devices_list.append(port[0])
        self.deviceComboBox.addItems(devices_list)

    def heading_update_devices(self):
        """ Load available ports and update device ComboBox """
        detector = QgsGpsDetector('scan')
        self.heading_deviceComboBox.clear()
        devices_list = []
        for port in detector.availablePorts():
            devices_list.append(port[0])
        self.heading_deviceComboBox.addItems(devices_list)

    def is_serial(self):
        """
        return True if serial RadioButton is checked, otherwise False
        :return: True if serial RadioButton is checked, otherwise False
        :rtype: bool
        """
        if self.serialPortRadioButton.isChecked():
            return True
        elif self.TCPRadioButton.isChecked():
            return False

    def heading_is_serial(self):
        """
        return True if serial RadioButton of heading is checked, otherwise False
        :return: True if serial RadioButton of heading is checked, otherwise False
        :rtype: bool
        """
        if self.heading_serialPortRadioButton.isChecked():
            return True
        elif self.heading_TCPRadioButton.isChecked():
            return False

    @property
    def configure_heading(self):
        """
        Return configure heading
        :return: configure heading
        :rtype: str
        """
        self.__configure_heading = self.configure_radioButton.isChecked()
        return self.__configure_heading

    @configure_heading.setter
    def configure_heading(self, configure_heading):
        """
        Set configure heading state
        :param configure_heading: new configure heading state
        :type configure_heading: str
        """
        self.__configure_heading = configure_heading
        self.configure_radioButton.setChecked(configure_heading)

    @property
    def imu_heading(self):
        """
        Return configure heading
        :return: configure heading
        :rtype: str
        """
        self.__imu_heading = self.usbl_imu_radioButton.isChecked()
        return self.__imu_heading

    @imu_heading.setter
    def imu_heading(self, imu_heading):
        """
        Set configure heading state
        :param imu_heading: new configure heading state
        :type imu_heading: str
        """
        self.__imu_heading = imu_heading
        self.usbl_imu_radioButton.setChecked(imu_heading)

    @property
    def serial_port(self):
        """
        Return serial port
        :return: serial port
        :rtype: str
        """
        self.__serial_port = self.deviceComboBox.currentText()
        return self.__serial_port

    @serial_port.setter
    def serial_port(self, serial_port):
        """
        Set serial port
        :param serial_port: new serial port data
        :type serial_port: str
        """
        self.__serial_port = serial_port
        self.deviceComboBox.setCurrentText(serial_port)

    @property
    def heading_serial_port(self):
        """
        Return heading serial port
        :return: heading serial port
        :rtype: str
        """
        self.__serial_port = self.heading_deviceComboBox.currentText()
        return self.__serial_port

    @heading_serial_port.setter
    def heading_serial_port(self, heading_serial_port):
        """
        Set serial port
        :param heading_serial_port: new serial port data
        :type heading_serial_port: str
        """
        self.__heading_serial_port = heading_serial_port
        self.heading_deviceComboBox.setCurrentText(heading_serial_port)

    @property
    def serial_baudrate(self):
        """
        Return serial baudrate
        :return: serial baudrate
        :rtype: int
        """
        self.__serial_baudrate = int(self.baudRateComboBox.currentText())
        return self.__serial_baudrate

    @serial_baudrate.setter
    def serial_baudrate(self, serial_baudrate):
        """
        Set serial baudrate
        :param serial_baudrate: new serial baudrate data
        :type serial_baudrate: int
        """
        self.__serial_baudrate = serial_baudrate
        self.baudRateComboBox.setCurrentText(str(serial_baudrate))

    @property
    def heading_serial_baudrate(self):
        """
        Return heading serial baudrate
        :return: heading serial baudrate
        :rtype: int
        """
        self.__heading_serial_baudrate = int(self.heading_baudRateComboBox.currentText())
        return self.__heading_serial_baudrate

    @heading_serial_baudrate.setter
    def heading_serial_baudrate(self, heading_serial_baudrate):
        """
        Set heading serial baudrate
        :param heading_serial_baudrate: new serial baudrate data
        :type heading_serial_baudrate: int
        """
        self.__heading_serial_baudrate = heading_serial_baudrate
        self.heading_baudRateComboBox.setCurrentText(str(heading_serial_baudrate))

    @property
    def ip(self):
        """
        :return: return ip address
        :rtype: str
        """
        self.__ip = self.ip_text.text()
        return self.__ip

    @ip.setter
    def ip(self, ip):
        """
        Set ip address
        :param ip: new ip address
        :type ip: str
        """
        self.__ip = ip
        self.ip_text.setText(ip)

    def get_ip(self):

        return self.ip

    @property
    def heading_ip(self):
        """
        :return: return headingip address
        :rtype: str
        """
        self.__heading_ip = self.heading_ip_text.text()
        return self.__heading_ip

    @heading_ip.setter
    def heading_ip(self, heading_ip):
        """
        Set heading ip address
        :param heading_ip: new heading ip address
        :type heading_ip: str
        """
        self.__heading_ip = heading_ip
        self.heading_ip_text.setText(heading_ip)

    @property
    def hdt_port(self):
        """
        :return: return hdt port
        :rtype: int
        """
        self.__hdt_port = int(self.hdt_port_text.text())
        return self.__hdt_port

    @hdt_port.setter
    def hdt_port(self, port):
        """
        set hdt port
        :param port: new hdt port
        :type port: int
        """
        self.__hdt_port = port
        self.hdt_port_text.setText(str(port))

    @property
    def gga_port(self):
        """
        :return: return gga port
        :rtype: int
        """
        self.__gga_port = int(self.gga_port_text.text())
        return self.__gga_port

    @gga_port.setter
    def gga_port(self, port):
        """
        set gga port
        :param port: new gga port
        :type port: int
        """
        self.__gga_port = port
        self.gga_port_text.setText(str(port))

    @property
    def protocol(self):
        """
        :return: return protocol
        :rtype: str
        """
        self.__protocol = self.protocol_comboBox.currentText()
        return self.__protocol

    @protocol.setter
    def protocol(self, protocol):
        """
        set protocol
        :param protocol:
        :type protocol: str
        """
        self.__protocol = protocol
        if protocol is not None:
            self.protocol_comboBox.setCurrentIndex(self.protocol_comboBox.findText(protocol))

    @property
    def heading_protocol(self):
        """
        :return: return protocol
        :rtype: str
        """
        self.__heading_protocol = self.heading_protocol_comboBox.currentText()
        return self.__heading_protocol

    @heading_protocol.setter
    def heading_protocol(self, heading_protocol):
        """
        set heading protocol
        :param heading_protocol:
        :type heading_protocol: str
        """
        self.__heading_protocol = heading_protocol
        if heading_protocol is not None:
            self.heading_protocol_comboBox.setCurrentIndex(self.heading_protocol_comboBox.findText(heading_protocol))

    @property
    def imu_port(self):
        """
        :return: return gga port
        :rtype: int
        """
        self.__imu_port = int(self.imu_port_lineedit.text())
        return self.__imu_port

    @imu_port.setter
    def imu_port(self, port):
        """
        set gga port
        :param port: new gga port
        :type port: int
        """
        self.__imu_port = port
        self.imu_port_lineedit.setText(str(port))

    def ip_validator(self):
        """ Validate text(ip) of sender"""
        sender = self.sender()
        state = validate_ip(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def port_validator(self):
        """ Validate text(port) of sender"""
        sender = self.sender()
        state = validate_port(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def is_gps_valid(self):
        """ Check if gps data is valid"""
        valid = False
        if self.configure_radioButton.isChecked():
            return((self.serialPortRadioButton.isChecked()
                     or (self.TCPRadioButton.isChecked()
                         and validate_ip(self.ip_text.text()) == QValidator.Acceptable
                         and validate_port(self.gga_port_text.text()) == QValidator.Acceptable))
                   and (self.heading_serialPortRadioButton.isChecked()
                        or (self.heading_TCPRadioButton.isChecked()
                            and validate_ip(self.heading_ip_text.text()) == QValidator.Acceptable
                            and validate_port(self.hdt_port_text.text()) == QValidator.Acceptable)))
        else:
            return (self.serialPortRadioButton.isChecked()
                    or (self.TCPRadioButton.isChecked()
                        and validate_ip(self.ip_text.text()) == QValidator.Acceptable
                        and validate_port(self.hdt_port_text.text()) == QValidator.Acceptable
                        and validate_port(self.gga_port_text.text()) == QValidator.Acceptable))
