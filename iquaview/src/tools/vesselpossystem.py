"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to set vessel size and set GPS and USBL offsets
"""
from importlib import util

from PyQt5.QtWidgets import (QWidget,
                             QGraphicsScene,
                             QGraphicsPolygonItem,
                             QGraphicsEllipseItem,
                             QGraphicsLineItem,
                             QGraphicsSimpleTextItem,
                             QGraphicsPathItem,
                             QListWidgetItem)
from PyQt5.QtGui import QPolygonF, QColor, QPainter, QPainterPath
from PyQt5.QtCore import QPointF, Qt, pyqtSignal
from iquaview.src.ui.ui_vesselpossystem import Ui_VesselPositionSystemWidget


class VesselPositionSystem(QWidget, Ui_VesselPositionSystemWidget):
    update_vessel_signal = pyqtSignal()

    def __init__(self, config, parent=None):
        super(VesselPositionSystem, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Vessel Position System")

        self.config = config
        self.current_vessel_name = self.config.csettings['vessel_name']
        self.count = 1

        self.reload()

        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.usbl_x_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
            self.usbl_y_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
            self.usbl_z_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
            self.usbl_magnetic_declination_doubleSpinBox.valueChanged.connect(self.print_vessel)
        else:
            self.hide_widgets_on_layout(self.usbl_gridLayout)

        # connect spinbox
        self.vessel_length_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.vessel_width_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.vrp_x_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.vrp_y_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.gps_x_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.gps_y_offset_doubleSpinBox.valueChanged.connect(self.print_vessel)
        self.gps_heading_doubleSpinBox.valueChanged.connect(self.print_vessel)

        self.listWidget.currentItemChanged.connect(self.change_vessel_config)
        self.listWidget.itemChanged.connect(self.change_vessel_config)
        self.listWidget.itemChanged.connect(self.rename)

        self.add_config_pushButton.clicked.connect(lambda: self.add_new_config("vessel"))
        self.remove_config_pushButton.clicked.connect(self.remove_config)


        # set top scene
        self.top_scene = QGraphicsScene(self.top_graphicsView)
        self.top_graphicsView.setScene(self.top_scene)

        self.side_scene = QGraphicsScene(self.side_graphicsView)
        self.side_graphicsView.setScene(self.side_scene)

    def update_params(self):
        # Set maximum and minimum values
        self.vrp_x_offset_doubleSpinBox.setMaximum(500.0)
        self.vrp_y_offset_doubleSpinBox.setMaximum(500.0)
        self.gps_x_offset_doubleSpinBox.setMaximum(500.0)
        self.gps_y_offset_doubleSpinBox.setMaximum(500.0)

        self.vrp_x_offset_doubleSpinBox.setMinimum(-500.0)
        self.vrp_y_offset_doubleSpinBox.setMinimum(-500.0)
        self.gps_y_offset_doubleSpinBox.setMinimum(-500.0)
        self.gps_x_offset_doubleSpinBox.setMinimum(-500.0)

        self.vessel_length_doubleSpinBox.setMinimum(1.0)
        self.vessel_width_doubleSpinBox.setMinimum(1.0)

        # set config values
        self.set_spinbox_value(self.vessel_length_doubleSpinBox, "vessel_length")
        self.set_spinbox_value(self.vessel_width_doubleSpinBox, "vessel_width")
        self.set_spinbox_value(self.vrp_x_offset_doubleSpinBox, 'vrp_offset_x')
        self.set_spinbox_value(self.vrp_y_offset_doubleSpinBox, 'vrp_offset_y')
        self.set_spinbox_value(self.gps_x_offset_doubleSpinBox, 'gps_offset_x')
        self.set_spinbox_value(self.gps_y_offset_doubleSpinBox, 'gps_offset_y')
        self.set_spinbox_value(self.gps_heading_doubleSpinBox, 'gps_offset_heading')

        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.usbl_x_offset_doubleSpinBox.setMaximum(500.0)
            self.usbl_x_offset_doubleSpinBox.setMinimum(-500.0)
            self.set_spinbox_value(self.usbl_x_offset_doubleSpinBox, 'usbl_offset_x')
            self.usbl_y_offset_doubleSpinBox.setMaximum(500.0)
            self.usbl_y_offset_doubleSpinBox.setMinimum(-500.0)
            self.set_spinbox_value(self.usbl_y_offset_doubleSpinBox, 'usbl_offset_y')
            self.set_spinbox_value(self.usbl_z_offset_doubleSpinBox, 'usbl_offset_z')
            self.set_spinbox_value(self.usbl_magnetic_declination_doubleSpinBox, 'usbl_magnetic_declination')
        else:
            self.hide_widgets_on_layout(self.usbl_gridLayout)

    def reload(self):
        current_vessel_name = self.config.csettings['vessel_name']

        self.update_params()

        self.listWidget.clear()
        self.load_vessel_configs()

        pos = 0
        for i in range(self.listWidget.count()):
            if self.listWidget.item(i).text() == current_vessel_name:
                pos = i

        self.change_vessel_config(self.listWidget.item(pos))

    def hide_widgets_on_layout(self, layout):
        """
        Hide all widgets from layout
        :param layout: a layout
        """
        for i in reversed(range(layout.count())):
            item = layout.takeAt(i)
            widget = item.widget()
            if widget is not None:
                widget.hide()

    def set_spinbox_value(self, spinbox, value):
        try:
            spinbox.setValue(self.config.csettings['vessel_configuration'][self.mGroupBox.title()][value])
        except:
            spinbox.setValue(0.0)

    def print_vessel(self):
        """
        printvessel draw vessel, gps, usbl and vessel reference point on a qgraphicsscene

        """
        self.print_top_view()
        self.print_side_view()

    def print_top_view(self):
        """
        draw draw vessel, gps, usbl and vessel reference point on a top_graphicsView
         
        """
        # clear previous scene
        self.top_scene.clear()
        self.top_graphicsView.viewport().update()

        # get vessel spinbox values
        v_width = self.vessel_width_doubleSpinBox.value()
        v_length = self.vessel_length_doubleSpinBox.value()
        vrp_x_on_vessel = self.vrp_x_offset_doubleSpinBox.value()
        vrp_y_on_vessel = self.vrp_y_offset_doubleSpinBox.value()
        gps_x_on_vessel = self.gps_x_offset_doubleSpinBox.value()
        gps_y_on_vessel = self.gps_y_offset_doubleSpinBox.value()

        if util.find_spec('iquaview_evologics_usbl') is not None:
            usbl_x_on_vessel = self.usbl_x_offset_doubleSpinBox.value()
            usbl_y_on_vessel = self.usbl_y_offset_doubleSpinBox.value()
        else:
            usbl_x_on_vessel = 0
            usbl_y_on_vessel = 0
        # get width and height from view
        w_max = self.top_graphicsView.width()
        h_max = self.top_graphicsView.height()

        # set max
        if v_width > v_length:
            max_pix = v_width
        else:
            max_pix = v_length

        # set pixel ratio
        if w_max < h_max:
            pix_ratio = (w_max - 20) / max_pix
        else:
            pix_ratio = (h_max - 20) / max_pix

        vessel_front_point = QPointF(pix_ratio * v_width / 2, 0)
        vessel_starboard_point = QPointF(pix_ratio * v_width, pix_ratio * v_length / 2)
        vessel_babor_point = QPointF(0, pix_ratio * v_length / 2)
        vessel_stern_starboard_point = QPointF(pix_ratio * v_width, pix_ratio * v_length)
        vessel_stern_babor_point = QPointF(0, pix_ratio * v_length)

        # paint vessel
        painter_path = QPainterPath()
        painter_path.moveTo(vessel_front_point)
        # bezzier curve to draw bow of the vessel
        painter_path.cubicTo(
                                    QPointF(pix_ratio * v_width , pix_ratio * v_length / 4),
                                    QPointF(pix_ratio * v_width, pix_ratio * v_length / 4),
                                    vessel_starboard_point)
        painter_path.lineTo(vessel_stern_starboard_point)
        painter_path.lineTo(vessel_stern_babor_point)
        painter_path.lineTo(vessel_babor_point)
        # bezzier curve to draw bow of the vessel
        painter_path.cubicTo(
                                    QPointF(0, pix_ratio * v_length / 4),
                                    QPointF(0, pix_ratio * v_length / 4),
                                    vessel_front_point)

        self.item = QGraphicsPathItem(painter_path)

        # set brown color
        self.item.setBrush(QColor(210, 180, 140))

        x_origin_scene = (pix_ratio * v_width / 2)
        y_origin_scene = (pix_ratio * v_length / 2)

        # coordinate system
        line_x_coord = QGraphicsLineItem(x_origin_scene,
                                         0,
                                         x_origin_scene,
                                         y_origin_scene)
        line_y_coord = QGraphicsLineItem(x_origin_scene,
                                         y_origin_scene,
                                         pix_ratio * v_width,
                                         y_origin_scene)
        x_label = QGraphicsSimpleTextItem("X", line_x_coord)
        x_label.setPos(x_origin_scene - 10,
                       0 + 10)
        y_label = QGraphicsSimpleTextItem("Y", line_y_coord)
        y_label.setPos(pix_ratio * v_width - 20,
                       y_origin_scene)

        x_origin_scene += vrp_y_on_vessel * pix_ratio
        y_origin_scene += -vrp_x_on_vessel * pix_ratio

        # draw origin point
        origin = QGraphicsEllipseItem(x_origin_scene - 10,
                                      y_origin_scene - 10,
                                      20, 20)
        origin.setBrush(Qt.white)
        line_one_origin = QGraphicsLineItem(x_origin_scene - 10,
                                            y_origin_scene,
                                            x_origin_scene + 10,
                                            y_origin_scene)
        line_two_origin = QGraphicsLineItem(x_origin_scene,
                                            y_origin_scene - 10,
                                            x_origin_scene,
                                            y_origin_scene + 10)

        # gps position
        gps_circle = QGraphicsEllipseItem(x_origin_scene - 10 + gps_y_on_vessel * pix_ratio,
                                          y_origin_scene - 10 - gps_x_on_vessel * pix_ratio,
                                          20, 20)
        gps_circle.setBrush(QColor(143, 188, 143))

        line_one_gps = QGraphicsLineItem(x_origin_scene - 10 + gps_y_on_vessel * pix_ratio,
                                         y_origin_scene - gps_x_on_vessel * pix_ratio,
                                         x_origin_scene + 10 + gps_y_on_vessel * pix_ratio,
                                         y_origin_scene - gps_x_on_vessel * pix_ratio)
        line_two_gps = QGraphicsLineItem(x_origin_scene + gps_y_on_vessel * pix_ratio,
                                         y_origin_scene - 10 - gps_x_on_vessel * pix_ratio,
                                         x_origin_scene + gps_y_on_vessel * pix_ratio,
                                         y_origin_scene + 10 - gps_x_on_vessel * pix_ratio)
        # gps label
        gps_label = QGraphicsSimpleTextItem("GPS", gps_circle)
        gps_label.setPos(x_origin_scene - 10 + gps_y_on_vessel * pix_ratio,
                         y_origin_scene + 10 - gps_x_on_vessel * pix_ratio)

        if util.find_spec('iquaview_evologics_usbl') is not None:
            # usbl position
            usbl_circle = QGraphicsEllipseItem(x_origin_scene - 10 + usbl_y_on_vessel * pix_ratio,
                                               y_origin_scene - 10 - usbl_x_on_vessel * pix_ratio,
                                               20, 20)
            usbl_circle.setBrush(QColor(255, 99, 71))

            line_one_usbl = QGraphicsLineItem(x_origin_scene - 10 + usbl_y_on_vessel * pix_ratio,
                                              y_origin_scene - usbl_x_on_vessel * pix_ratio,
                                              x_origin_scene + 10 + usbl_y_on_vessel * pix_ratio,
                                              y_origin_scene - usbl_x_on_vessel * pix_ratio)
            line_two_usbl = QGraphicsLineItem(x_origin_scene + usbl_y_on_vessel * pix_ratio,
                                              y_origin_scene - 10 - usbl_x_on_vessel * pix_ratio,
                                              x_origin_scene + usbl_y_on_vessel * pix_ratio,
                                              y_origin_scene + 10 - usbl_x_on_vessel * pix_ratio)
            # usbl label
            usbl_label = QGraphicsSimpleTextItem("USBL", usbl_circle)
            if usbl_x_on_vessel == gps_x_on_vessel and usbl_y_on_vessel == gps_y_on_vessel:
                usbl_label.setPos(x_origin_scene - 10 + usbl_y_on_vessel * pix_ratio,
                                  y_origin_scene - 30 - usbl_x_on_vessel * pix_ratio)
            else:
                usbl_label.setPos(x_origin_scene - 10 + usbl_y_on_vessel * pix_ratio,
                                  y_origin_scene + 10 - usbl_x_on_vessel * pix_ratio)

        origin_label = QGraphicsSimpleTextItem("VRP", origin)
        if (usbl_x_on_vessel == 0 and usbl_y_on_vessel == 0) or (gps_x_on_vessel == 0 and gps_y_on_vessel == 0):
            origin_label.setPos(x_origin_scene + 10,
                                y_origin_scene - 10)
        else:
            origin_label.setPos(x_origin_scene - 10,
                                y_origin_scene + 10)

        # fit view/scene
        self.top_graphicsView.setSceneRect(0, 0, pix_ratio * v_width, pix_ratio * v_length)

        # add vessel to the scene
        self.top_graphicsView.scene().addItem(self.item)
        # add origin to the scene
        self.top_graphicsView.scene().addItem(origin)
        self.top_graphicsView.scene().addItem(line_one_origin)
        self.top_graphicsView.scene().addItem(line_two_origin)
        # add gps
        self.top_graphicsView.scene().addItem(gps_circle)
        self.top_graphicsView.scene().addItem(line_one_gps)
        self.top_graphicsView.scene().addItem(line_two_gps)

        if util.find_spec('iquaview_evologics_usbl') is not None:
            # add usbl
            self.top_graphicsView.scene().addItem(usbl_circle)
            self.top_graphicsView.scene().addItem(line_one_usbl)
            self.top_graphicsView.scene().addItem(line_two_usbl)

        # add coord system
        self.top_graphicsView.scene().addItem(line_x_coord)
        self.top_graphicsView.scene().addItem(line_y_coord)
        # set background
        self.top_scene.setBackgroundBrush(QColor(204, 229, 255))

        # set antialiasing renderhint
        self.top_graphicsView.setRenderHint(QPainter.Antialiasing)

    def print_side_view(self):
        """
        draw vessel, usbl and vessel reference point on a side_graphicsView
         
        """

        # clear previous scene
        self.side_scene.clear()
        self.side_graphicsView.viewport().update()

        # get vessel spinbox values
        v_length = self.vessel_length_doubleSpinBox.value()
        v_height = v_length / 4

        vrp_x_on_vessel = self.vrp_x_offset_doubleSpinBox.value()

        if util.find_spec('iquaview_evologics_usbl') is not None:
            usbl_x_on_vessel = self.usbl_x_offset_doubleSpinBox.value()
            usbl_z_on_vessel = self.usbl_z_offset_doubleSpinBox.value()
        else:
            usbl_x_on_vessel = 0
            usbl_z_on_vessel = 0

        # get width and height from view
        w_max = self.side_graphicsView.width()
        h_max = self.side_graphicsView.height()

        # set max
        if v_height > v_length:
            max_pix = v_height
        else:
            max_pix = v_length

        # set pixel ratio
        if w_max < h_max:
            pix_ratio = (w_max - 20) / max_pix
        else:
            pix_ratio = (h_max - 20) / max_pix

        # set the size of the vessel
        vessel_stern_top_point = QPointF(0, 0)
        vessel_stern_down_point = QPointF(0, pix_ratio * v_height)
        vessel_front_top_point =  QPointF(pix_ratio * v_length,  0)
        vessel_bow_point = QPointF(pix_ratio * v_length * 0.6, pix_ratio * v_height)


        # paint vessel
        painter_path = QPainterPath()
        painter_path.moveTo(vessel_stern_top_point)
        # bezzier curve to draw vessel
        # boat length
        painter_path.cubicTo( QPointF(pix_ratio * v_length * 0.25,  0 ),
                              QPointF(pix_ratio * v_length * 1, 0),
                              vessel_front_top_point)
        # bow
        painter_path.cubicTo(QPointF(pix_ratio * v_length * 0.8, pix_ratio * v_height),
                             QPointF(pix_ratio * v_length * 0.8, pix_ratio * v_height),
                             vessel_bow_point)
        painter_path.lineTo(vessel_stern_down_point)
        # stern
        painter_path.lineTo(vessel_stern_top_point)

        self.item = QGraphicsPathItem(painter_path)


        # set brown color
        self.item.setBrush(QColor(210, 180, 140))

        x_origin_scene = (pix_ratio * v_length / 2)
        y_origin_scene = (pix_ratio * v_height / 2)

        # coordinate system
        line_x_coord = QGraphicsLineItem(x_origin_scene,
                                         y_origin_scene,
                                         pix_ratio * v_length - (
                                                 pix_ratio * v_length - pix_ratio * v_length) / 2,
                                         y_origin_scene)
        line_z_coord = QGraphicsLineItem(x_origin_scene,
                                         y_origin_scene,
                                         x_origin_scene,
                                         pix_ratio * v_height)
        x_label = QGraphicsSimpleTextItem("X", line_x_coord)
        x_label.setPos(pix_ratio * v_length - (pix_ratio * v_length - pix_ratio * v_length * 3 / 4) / 2 - 30,
                       y_origin_scene)
        z_label = QGraphicsSimpleTextItem("Z", line_z_coord)
        z_label.setPos(x_origin_scene - 20,
                       pix_ratio * v_height - 20)

        # set sea background
        sea_polygon = QPolygonF([QPointF(-w_max, y_origin_scene),
                                 QPointF(w_max, y_origin_scene),
                                 QPointF(x_origin_scene + w_max, h_max + usbl_z_on_vessel * pix_ratio),
                                 QPointF(-w_max, h_max + usbl_z_on_vessel * pix_ratio)])
        sea = QGraphicsPolygonItem(sea_polygon)
        sea.setBrush(QColor(204, 229, 255))

        x_origin_scene += vrp_x_on_vessel * pix_ratio

        # draw origin point
        origin = QGraphicsEllipseItem(x_origin_scene - 10,
                                      y_origin_scene - 10,
                                      20, 20)
        origin.setBrush(Qt.white)
        line_one_origin = QGraphicsLineItem(x_origin_scene - 10,
                                            y_origin_scene,
                                            x_origin_scene + 10,
                                            y_origin_scene)
        line_two_origin = QGraphicsLineItem(x_origin_scene,
                                            y_origin_scene - 10,
                                            x_origin_scene,
                                            y_origin_scene + 10)

        if util.find_spec('iquaview_evologics_usbl') is not None:
            # usbl position
            usbl_circle = QGraphicsEllipseItem(x_origin_scene - 10 + usbl_x_on_vessel * pix_ratio,
                                               y_origin_scene - 10 + usbl_z_on_vessel * pix_ratio,
                                               20, 20)
            usbl_circle.setBrush(QColor(255, 99, 71))

            line_one_usbl = QGraphicsLineItem(x_origin_scene - 10 + usbl_x_on_vessel * pix_ratio,
                                              y_origin_scene + usbl_z_on_vessel * pix_ratio,
                                              x_origin_scene + 10 + usbl_x_on_vessel * pix_ratio,
                                              y_origin_scene + usbl_z_on_vessel * pix_ratio)
            line_two_usbl = QGraphicsLineItem(x_origin_scene + usbl_x_on_vessel * pix_ratio,
                                              y_origin_scene - 10 + usbl_z_on_vessel * pix_ratio,
                                              x_origin_scene + usbl_x_on_vessel * pix_ratio,
                                              y_origin_scene + 10 + usbl_z_on_vessel * pix_ratio)

            # define labels

            usbl_label = QGraphicsSimpleTextItem("USBL", usbl_circle)
            usbl_label.setPos(x_origin_scene - 10 + usbl_x_on_vessel * pix_ratio,
                              y_origin_scene + 10 + usbl_z_on_vessel * pix_ratio)

        origin_label = QGraphicsSimpleTextItem("VRP", origin)
        if usbl_x_on_vessel == 0 and usbl_z_on_vessel == 0:
            origin_label.setPos(x_origin_scene + 10,
                                y_origin_scene - 10)
        else:
            origin_label.setPos(x_origin_scene - 10,
                                y_origin_scene + 10)

        # fit view/scene
        self.side_scene.setSceneRect(0, 0, pix_ratio * v_length, pix_ratio * v_height + usbl_z_on_vessel * pix_ratio)

        self.side_graphicsView.scene().addItem(sea)

        # add vessel to the scene
        self.side_graphicsView.scene().addItem(self.item)
        # add origin to the scene
        self.side_graphicsView.scene().addItem(origin)
        self.side_graphicsView.scene().addItem(line_one_origin)
        self.side_graphicsView.scene().addItem(line_two_origin)

        if util.find_spec('iquaview_evologics_usbl') is not None:
            # add usbl
            self.side_graphicsView.scene().addItem(usbl_circle)
            self.side_graphicsView.scene().addItem(line_one_usbl)
            self.side_graphicsView.scene().addItem(line_two_usbl)

        # add coord system
        self.side_graphicsView.scene().addItem(line_x_coord)
        self.side_graphicsView.scene().addItem(line_z_coord)

        # set background
        self.side_scene.setBackgroundBrush(Qt.white)

        # set antialiasing renderhint
        self.side_graphicsView.setRenderHint(QPainter.Antialiasing)

    def change_vessel_config(self, item: QListWidgetItem):
        """
        Set item as current vessel config
        :param item: item from list widget
        :type item: QListWidgetItem
        """
        if item is None:
            item = self.listWidget.item(0)

        if item is not None:
            self.mGroupBox.setTitle(item.text())
            self.current_vessel_name = self.mGroupBox.title()
            self.update_params()

        if self.listWidget.count() <= 1:
            self.remove_config_pushButton.setEnabled(False)
        else:
            self.remove_config_pushButton.setEnabled(True)

    def rename(self, item: QListWidgetItem):
        """
        rename item
        :param item: item from list widget
        :type item: QListWidgetItem
        """
        occurrences = 0
        for i in range(self.listWidget.count()):
            if self.listWidget.item(i).text() == item.text():
                occurrences += 1

        if occurrences > 1:
            temp_key = item.text()
            while self.check_name(temp_key) is True:
                self.count += 1
                temp_key = item.text() + "_" + str(self.count)

            item.setText(temp_key)

    def check_name(self, key):
        """
        check if item with key exists in config list
        :param key: key of item
        :type key: str
        :return: return True if item with name key exists, otherwise return False
        """
        for i in range(self.listWidget.count()):
            if self.listWidget.item(i).text() == key:
                return True

        return False

    def add_new_config(self, key="vessel"):
        """
        Add ned configuration with name 'key'
        :param key: new key
        :type key: str
        """
        temp_key = key
        while self.check_name(temp_key) is True:
            self.count += 1
            temp_key = key + "_" + str(self.count)

        section_item = QListWidgetItem()
        section_item.setFlags(section_item.flags() | Qt.ItemIsEditable)
        section_item.setText(temp_key)
        self.listWidget.addItem(section_item)
        # self.treeWidget.edit(self.treeWidget.indexFromItem(section_item))

        if self.listWidget.count() <= 1:
            self.remove_config_pushButton.setEnabled(False)
        else:
            self.remove_config_pushButton.setEnabled(True)

    def remove_config(self):
        """
        Remove current item of configuration
        """
        self.listWidget.takeItem(self.listWidget.indexFromItem(self.listWidget.currentItem()).row())

        if self.listWidget.count() <= 1:
            self.remove_config_pushButton.setEnabled(False)
        else:
            self.remove_config_pushButton.setEnabled(True)

    def load_vessel_configs(self):
        """ Load vessel configurations from csettings"""
        if (self.config.csettings.get('vessel_configuration')
                and type(self.config.csettings.get('vessel_configuration')) == dict):
            for key, item in self.config.csettings.get('vessel_configuration').items():
                if item:
                    self.add_new_config(key)
            self.mGroupBox.setTitle(self.current_vessel_name)

        else:
            #set default
            self.add_new_config("default")
            self.current_vessel_name = "default"
            self.mGroupBox.setTitle(self.current_vessel_name)
            self.vessel_width_doubleSpinBox.setValue(7.0)
            self.vessel_length_doubleSpinBox.setValue(15.0)

            self.vrp_x_offset_doubleSpinBox.setValue(0.0)
            self.vrp_y_offset_doubleSpinBox.setValue(0.0)
            self.gps_x_offset_doubleSpinBox.setValue(4.0)
            self.gps_y_offset_doubleSpinBox.setValue(3.0)
            self.gps_heading_doubleSpinBox.setValue(0.0)

            if util.find_spec('iquaview_evologics_usbl') is not None:
                self.usbl_x_offset_doubleSpinBox.setValue(-3.0)
                self.usbl_y_offset_doubleSpinBox.setValue(-2.0)
                self.usbl_z_offset_doubleSpinBox.setValue(0.0)
                self.usbl_magnetic_declination_doubleSpinBox.setValue(0.0)
            self.on_apply()
            self.change_vessel_config(None)

    def on_apply(self):
        """On apply set values on config csettings"""
        self.config.csettings['vessel_name'] = self.current_vessel_name
        if self.config.csettings.get("vessel_configuration") is not None:
            for key, value in self.config.csettings.get("vessel_configuration").items():
                found = self.check_name(key)
                if not found:
                    self.config.csettings["vessel_configuration"][key] = {}
        else:
            self.config.csettings["vessel_configuration"] = {}

        self.config.csettings['vessel_configuration'][self.mGroupBox.title()] = {}

        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]["vessel_width"] = self.vessel_width_doubleSpinBox.value()
        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]["vessel_length"] = self.vessel_length_doubleSpinBox.value()

        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]["vrp_offset_x"] = self.vrp_x_offset_doubleSpinBox.value()
        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]["vrp_offset_y"] = self.vrp_y_offset_doubleSpinBox.value()

        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_x'] = self.gps_x_offset_doubleSpinBox.value()
        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_y'] = self.gps_y_offset_doubleSpinBox.value()
        self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_heading'] = self.gps_heading_doubleSpinBox.value()

        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_x'] = self.usbl_x_offset_doubleSpinBox.value()
            self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_y'] = self.usbl_y_offset_doubleSpinBox.value()
            self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_z'] = self.usbl_z_offset_doubleSpinBox.value()
            self.config.csettings['vessel_configuration'][self.mGroupBox.title()]['usbl_magnetic_declination'] = self.usbl_magnetic_declination_doubleSpinBox.value()

        self.update_vessel_signal.emit()

    def on_accept(self):
        """On accept set values on config settings"""

        self.on_apply()

        self.config.settings['vessel_name'] = self.current_vessel_name

        if self.config.settings.get("vessel_configuration") is not None:
            for key, value in self.config.settings.get("vessel_configuration").items():
                found = self.check_name(key)
                if not found:
                    self.config.settings["vessel_configuration"][key] = {}
        else:
            self.config.settings["vessel_configuration"] = {}

        self.config.settings['vessel_configuration'][self.mGroupBox.title()] = {}

        self.config.settings['vessel_configuration'][self.mGroupBox.title()]["vessel_width"] = self.vessel_width_doubleSpinBox.value()
        self.config.settings['vessel_configuration'][self.mGroupBox.title()]["vessel_length"] = self.vessel_length_doubleSpinBox.value()

        self.config.settings['vessel_configuration'][self.mGroupBox.title()]["vrp_offset_x"] = self.vrp_x_offset_doubleSpinBox.value()
        self.config.settings['vessel_configuration'][self.mGroupBox.title()]["vrp_offset_y"] = self.vrp_y_offset_doubleSpinBox.value()

        self.config.settings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_x'] = self.gps_x_offset_doubleSpinBox.value()
        self.config.settings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_y'] = self.gps_y_offset_doubleSpinBox.value()
        self.config.settings['vessel_configuration'][self.mGroupBox.title()]['gps_offset_heading'] = self.gps_heading_doubleSpinBox.value()

        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.config.settings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_x'] = self.usbl_x_offset_doubleSpinBox.value()
            self.config.settings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_y'] = self.usbl_y_offset_doubleSpinBox.value()
            self.config.settings['vessel_configuration'][self.mGroupBox.title()]['usbl_offset_z'] = self.usbl_z_offset_doubleSpinBox.value()
            self.config.settings['vessel_configuration'][self.mGroupBox.title()]['usbl_magnetic_declination'] = self.usbl_magnetic_declination_doubleSpinBox.value()

        self.config.save()
        return True

    def resizeEvent(self, qresizeevent):
        super(VesselPositionSystem, self).resizeEvent(qresizeevent)
        self.print_vessel()
