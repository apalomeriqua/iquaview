function getColor(d) {
    return d === 'Vehicle' ? '#ffff00' :
        d === 'Vessel' ? '#008000' :
        d === 'USBL' ? '#ff0000' :
        '#ff7f00';
}

function style(feature) {
    return {
        weight: 1.5,
        opacity: 1,
        fillOpacity: 1,
        radius: 6,
        fillColor: getColor(feature.properties.TypeOfIssue),
        color: "grey"

    };
}

$(document).ready(function() {

    var vessel_width = 0;
    var vessel_length = 0;
    var vehicle_namespace = "";

    var labels = ['<strong>Positions</strong>'];
    var categories = ['Vehicle', 'Vessel', 'USBL', ];

    var USBLConnected = false;
    var GPSConnected = false;
    var AUVConnected = false;

    var map = L.map('map').setView([41.77, 3.033], 14);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        maxNativeZoom: 19,
        maxZoom: 24
    }).addTo(map);

    L.control.scale().addTo(map);

    var div_pos = L.DomUtil.create('div', 'info legend_pos');
    var div_depth = L.DomUtil.create('div', 'info legend_pos');

    var vehicle = L.marker([0, 0], {rotationAngle: 0}).addTo(map);
    var vessel = L.marker([0, 0], {rotationAngle: 0}).addTo(map);
    var usbl = L.marker([0, 0], {rotationAngle: 0}).addTo(map);

    var vehicle_disconnected = L.marker([0, 0], {rotationAngle: 0}).addTo(map);
    var vessel_disconnected = L.marker([0, 0], {rotationAngle: 0}).addTo(map);
    var usbl_disconnected = L.marker([0, 0], {rotationAngle: 0}).addTo(map);

    var legend_pos = L.control({ position: 'bottomleft'});

    legend_pos.onAdd = function(map) {

        for (var i = 0; i < categories.length; i++) {
            div_pos.innerHTML +=
                labels.push(
                    '<i id=' + categories[i] + ' class="dot" style="background:' + getColor(categories[i]) + '"></i> ' +
                    (categories[i] ? categories[i] : '+'));
        }
        div_pos.innerHTML = labels.join('<br>');

        return div_pos;
    };
    legend_pos.addTo(map);

    for (var i = 0; i < categories.length; i++) {
        var dot_category = document.getElementById(categories[i]);
        dot_category.style.cursor = "pointer";
        dot_category.onclick = function() {
            if (this.id === 'USBL') {
                latlng = usbl.getLatLng();
            } else if (this.id === 'Vessel') {
                latlng = vessel.getLatLng();
            } else {
                //Vehicle
                latlng = vehicle.getLatLng();
            }
            map.setView(latlng, map.getZoom());
        };
    };

    var legend_altitude = L.control({
        position: 'bottomright'
    });
    legend_altitude.onAdd = function(map) {

        var div_depth = L.DomUtil.create('div', 'info legend_altitude');
        labels = ['<strong>Vehicle Depth</strong>'],

            div_depth.innerHTML +=
            labels.push(
                '<i id="vehicle_depth" style="background: ">- m</i> ');

        div_depth.innerHTML = labels.join('<br>');
        return div_depth;
    };
    legend_altitude.addTo(map);

    var legend_depth = L.control({
        position: 'bottomright'
    });
    legend_depth.onAdd = function(map) {

        var div_depth = L.DomUtil.create('div', 'info legend_depth');
        labels = ['<strong>Vehicle Altitude</strong>'],

            div_depth.innerHTML +=
            labels.push(
                '<i id="vehicle_altitude" style="background: ">- m</i> ');

        div_depth.innerHTML = labels.join('<br>');
        return div_depth;
    };
    legend_depth.addTo(map);

    var USBLIcon = resizeIcon(usbl, 0, 0, 20, 20, 'static/images/usbl.svg', 'static/images/usbl.svg');
    var vesselIcon = resizeIcon(vessel, vessel_width, vessel_length, 20, 28, 'static/images/vessel.svg', 'static/images/vessel_icon.svg');
    var vehicleIcon = resizeIcon(vehicle, 2, 2, 20, 28, 'static/images'+ vehicle_namespace +'/vehicle.svg', 'static/images/vehicle_icon.svg');

    usbl.setIcon(USBLIcon);
    vessel.setIcon(vesselIcon);
    vehicle.setIcon(vehicleIcon);

    var USBLIconDisconnected = resizeIcon(usbl_disconnected, 0, 0, 20, 20, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');
    var vesselIconDisconnected = resizeIcon(vessel_disconnected, vessel_width, vessel_length, 20, 28, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');
    var vehicleIconDisconnected = resizeIcon(vehicle_disconnected, 2, 2, 20, 28, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');

    usbl_disconnected.setIcon(USBLIconDisconnected);
    vessel_disconnected.setIcon(vesselIconDisconnected);
    vehicle_disconnected.setIcon(vehicleIconDisconnected);
    usbl_disconnected.setOpacity(0);
    vessel_disconnected.setOpacity(0);
    vehicle_disconnected.setOpacity(0);


    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/viewer');
    var numbers_received = [];

    //receive details from server
    socket.on('new_vehicle_position', function(msg) {

        var lat = (msg.latitude);
        var lng = (msg.longitude);
        var heading = (msg.heading);
        var depth = (msg.depth);
        var altitude = (msg.altitude);
        vehicle_namespace = (msg.vehicle_namespace);
        var newLatLng = new L.LatLng(lat, lng);
        vehicle.setLatLng(newLatLng);
        document.getElementById("vehicle_depth").innerHTML = Math.round((depth + Number.EPSILON) * 100) / 100 + " m"
        document.getElementById("vehicle_altitude").innerHTML = Math.round((altitude + Number.EPSILON) * 100) / 100 + " m"

        var vehicleIcon = resizeIcon(vehicle, 2, 2, 20, 28, 'static/images'+ vehicle_namespace +'/vehicle.svg', 'static/images/vehicle_icon.svg');
        vehicle.setIcon(vehicleIcon);

        vehicle.setRotationOrigin(vehicle.options.rotationAngle);
        vehicle.setRotationAngle(heading);

        AUVConnected = true;

        vehicle_disconnected.setOpacity(0);
        vehicle_disconnected.setLatLng(newLatLng);
        vehicle_disconnected.setRotationOrigin(vessel.options.rotationAngle);
        vehicle_disconnected.setRotationAngle(heading);
    });

    //receive details from server
    socket.on('new_vessel_position', function(msg) {

        var lat = (msg.latitude);
        var lng = (msg.longitude);
        var heading = (msg.heading);
        vessel_width = (msg.vessel_width);
        vessel_length = (msg.vessel_length);
        var newLatLng = new L.LatLng(lat, lng);
        vessel.setLatLng(newLatLng);

        var vesselIcon = resizeIcon(vessel, vessel_width, vessel_length, 20, 28, 'static/images/vessel.svg', 'static/images/vessel_icon.svg');
        vessel.setIcon(vesselIcon);

        vessel.setRotationOrigin(vessel.options.rotationAngle);
        vessel.setRotationAngle(heading);

        GPSConnected = true;

        vessel_disconnected.setOpacity(0);
        vessel_disconnected.setLatLng(newLatLng);
        vessel_disconnected.setRotationOrigin(vessel.options.rotationAngle);
        vessel_disconnected.setRotationAngle(heading);
    });

    //receive details from server
    socket.on('new_usbl_position', function(msg) {

        var lat = (msg.latitude);
        var lng = (msg.longitude);
        var heading = (msg.heading);
        var newLatLng = new L.LatLng(lat, lng);
        usbl.setLatLng(newLatLng);

        var USBLIcon = resizeIcon(usbl, 0, 0, 20, 20, 'static/images/usbl.svg', 'static/images/usbl.svg');
        usbl.setIcon(USBLIcon);

        usbl.setRotationOrigin(usbl.options.rotationAngle);
        usbl.setRotationAngle(heading);

        USBLConnected = true;
        usbl_disconnected.setOpacity(0);
        usbl_disconnected.setLatLng(newLatLng);
        usbl_disconnected.setRotationOrigin(usbl.options.rotationAngle);
        usbl_disconnected.setRotationAngle(heading);

    });
    socket.on('auv_no_signal', function(msg) {
        AUVConnected = false;
//        vehicleIconDisconnected = resizeIcon(vehicle_disconnected, 2, 2, 20, 28, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');
//        vehicle.setIcon(vehicleIconDisconnected);
        vehicle_disconnected.setOpacity(100);

    });
        socket.on('vessel_no_signal', function(msg) {
//        vesselIconDisconnected = resizeIcon(vessel_disconnected, 2, 2, 20, 28, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');
//        vessel_disconnected.setIcon(vesselIconDisconnected);
        GPSConnected = false;
        vessel_disconnected.setOpacity(100);

    });
        socket.on('usbl_no_signal', function(msg) {
//        USBLIconDisconnected = resizeIcon(usbl_disconnected, 2, 2, 20, 28, 'static/images/connectionLost.svg', 'static/images/connectionLost.svg');
//        usbl_disconnected.setIcon(USBLIconDisconnected);
        USBLConnected = false;
        usbl_disconnected.setOpacity(100);

    });

    map.on('zoomend', function() {

        var USBLIcon = resizeIcon(usbl, 0, 0, 20, 20, 'static/images/usbl.svg', 'static/images/usbl.svg');
        var vesselIcon = resizeIcon(vessel, vessel_width, vessel_length, 20, 28, 'static/images/vessel.svg', 'static/images/vessel_icon.svg');
        var vehicleIcon = resizeIcon(vehicle, 2, 2, 20, 28, 'static/images'+ vehicle_namespace +'/vehicle.svg', 'static/images/vehicle_icon.svg');

        usbl.setIcon(USBLIcon);
        vessel.setIcon(vesselIcon);
        vehicle.setIcon(vehicleIcon);
    });


    function resizeIcon(marker, width, length, x_default_icon_size, y_default_icon_size, url_image, url_icon) {

        var url = url_image;
        var centerLatLng = marker.getLatLng();
        var pointC = map.latLngToContainerPoint(centerLatLng); // convert to containerpoint (pixels)
        var pointX = [pointC.x, pointC.y]; // add one pixel to x
        var pointY = [pointC.x, pointC.y]; // add one pixel to y

        var distanceX = 0;
        var distanceY = 0;
        let i = 1;
        var X_size = i * 2;
        var Y_size = i * 2;
        while (distanceX < width || distanceY < length) {
            if (distanceX < width) {
                var pointX = [pointC.x + i, pointC.y]; // add one pixel to x
                var X_size = i;
            }
            if (distanceY < length) {
                var pointY = [pointC.x, pointC.y + i]; // add one pixel to y
                var Y_size = i;
            }

            // convert containerpoints to latlng's
            var latLngC = map.containerPointToLatLng(pointC);
            var latLngX = map.containerPointToLatLng(pointX);
            var latLngY = map.containerPointToLatLng(pointY);

            var distanceX = latLngC.distanceTo(latLngX); // calculate distance between c and x (latitude)
            var distanceY = latLngC.distanceTo(latLngY); // calculate distance between c and y (longitude)
            i++;
        }

        if (width == 0 || length == 0 || (X_size < x_default_icon_size && Y_size < y_default_icon_size)) {
            X_size = 20;
            Y_size = 28;
            anchor_X = X_size / 2;
            anchor_Y = X_size / 2;
            url = url_icon;
        } else {
            anchor_X = X_size / 2;
            anchor_Y = Y_size / 2;
        }

        var newIcon = L.icon({
            iconUrl: url,
            iconSize: [X_size, Y_size], // size of the icon
            iconAnchor: [anchor_X, anchor_Y] // point of the icon which will correspond to marker's location
            //        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        //        console.log(width+ " "+ length +" "+ X_size + " " +Y_size + " " + anchor_X + " " + anchor_Y);
        return newIcon;
    }
});