"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import math

# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit
from flask import Flask, render_template
from threading import Thread, Event

from PyQt5.QtCore import QThreadPool, pyqtSignal
from PyQt5.QtWidgets import QWidget

from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.cola2api.gps_driver import GPSPosition, GPSOrientation
from iquaview_lib.cola2api.basic_message import BasicMessage
from iquaview_lib.utils.calcutils import endpoint_sphere
from iquaview_lib.utils.switchbuttonwidget import SwitchButton
from iquaview_lib.utils.textvalidator import (validate_port,
                                              get_color,
                                              get_int_validator)
from iquaview_lib.config import Config


from iquaview.src.ui.ui_web_server import Ui_WebServerWidget

logger = logging.getLogger(__name__)
logging.getLogger('werkzeug').setLevel(logging.ERROR)
logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)


class WebServer(QWidget, Ui_WebServerWidget):

    stop_server_signal = pyqtSignal()

    global app
    app = Flask(__name__)

    def __init__(self, config: Config, vehicle_data: VehicleData, vehicle_info: VehicleInfo):
        super(WebServer, self).__init__()
        self.setupUi(self)
        self.switch_button = SwitchButton(self, "On", 10, "Off", 25, 60)
        self.state_hboxlayout.addWidget(self.switch_button)

        self.port_text.setValidator(get_int_validator(0, 65535))
        self.port_text.textChanged.connect(self.port_validator)

        self.config = config
        self.vehicle_data = vehicle_data
        self.vehicle_info = vehicle_info
        self.usbl_data = BasicMessage()
        self.auv_data = BasicMessage()
        self.gps_position = GPSPosition(time=0.0, latitude=0.0, longitude=0.0, altitude=0.0, quality=-1)
        self.gps_orientation = GPSOrientation(time=0.0, orientation_deg=0.0)

        # turn the flask app into a socketio app
        self.socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=True)
        self.socketio.on_event('connect', self.connect, namespace='/viewer')
        self.socketio.on_event('disconnect', self.disconnect, namespace='/viewer')
        self.stop_server_signal.connect(self.stop_server)

        # Thread
        self.thread = None
        self.thread_stop_event = Event()
        self.threadpool = QThreadPool()

        self.reload()
        self.on_apply()

    def reload(self):
        self.switch_button.set_state(self.config.csettings.get('web_server_state'))
        self.port_text.setText(str(self.config.csettings.get('web_server_port')))

    def on_apply(self):
        self.config.csettings['web_server_state'] = self.switch_button.is_on()
        self.config.csettings['web_server_port'] = int(self.port_text.text())

        self.stop_server_signal.emit()

        if self.switch_button.is_on():
            self.start_server_thread()

    def on_accept(self):
        self.on_apply()
        self.config.settings['web_server_state'] = self.switch_button.is_on()
        self.config.settings['web_server_port'] = int(self.port_text.text())
        self.config.save()

    def port_validator(self):
        """ Validate text(port) of sender"""
        sender = self.sender()
        state = validate_port(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def start_server_thread(self):
        worker = Worker(self.run_server)  # Any other args, kwargs are passed to the run function
        self.threadpool.start(worker)

    def run_server(self):

        self.socketio.run(app, host="0.0.0.0", port=int(self.port_text.text()))

    def stop_server(self):
        try:
            # gevent stop socketio run
            self.socketio.stop()
        except Exception as e:
            logger.error("Error stopping socketio {}".format(e))

        try:
            self.threadpool.clear()
        except Exception as e:
            logger.error("Error stopping threadpool {}".format(e))

    def position_generator(self):
        """
        emit to a socketio instance (broadcast)
        Ideally to be run in a separate thread?
        """

        while not self.thread_stop_event.isSet():
            if self.vehicle_data.is_subscribed():
                vehicle_data = self.vehicle_data.get_nav_sts()
                if vehicle_data is not None and vehicle_data['valid_data'] == 'new_data':
                    vehicle_lat = float(vehicle_data['global_position']['latitude'])
                    vehicle_lon = float(vehicle_data['global_position']['longitude'])
                    vehicle_heading = math.degrees(float(vehicle_data['orientation']['yaw']))
                    vehicle_depth = float(vehicle_data['position']['depth'])
                    vehicle_altitude = float(vehicle_data['altitude'])
                    self.socketio.emit('new_vehicle_position', {'latitude': vehicle_lat,
                                                           'longitude': vehicle_lon,
                                                           'heading': vehicle_heading,
                                                           'depth': vehicle_depth,
                                                           'altitude': vehicle_altitude,
                                                           'vehicle_namespace': self.vehicle_info.get_vehicle_namespace()}, namespace='/viewer')


            elif (self.auv_data.time is not None and self.auv_data.time != 0.0):
                self.socketio.emit('new_vehicle_position', {'latitude': self.auv_data.latitude,
                                                            'longitude': self.auv_data.longitude,
                                                            'heading': self.auv_data.heading_accuracy,
                                                            'depth': self.auv_data.depth,
                                                            'altitude': self.auv_data.altitude,
                                                            'vehicle_namespace': self.vehicle_info.get_vehicle_namespace()}, namespace='/viewer')


            else:
                self.socketio.emit('auv_no_signal', {}, namespace='/viewer')

            if self.gps_position.is_new() and (self.gps_position.quality >= 1) and (self.gps_position.quality <= 5):
                vessel_lat = self.gps_position.latitude
                vessel_lon = self.gps_position.longitude

                if self.gps_orientation.is_new():
                    vessel_heading = self.gps_orientation.orientation_deg
                else:
                    vessel_heading = 0

                vessel_name = self.config.csettings['vessel_name']

                vessel_width = self.config.csettings['vessel_configuration'][vessel_name]["vessel_width"]
                vessel_length = self.config.csettings['vessel_configuration'][vessel_name]["vessel_length"]

                vessel_name = self.config.csettings['vessel_name']

                brng = (math.radians(vessel_heading - self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_heading'])
                        + math.atan2(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                         + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                                     self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                         + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x']))
                distance = - math.sqrt(
                    math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_x']
                                 + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_x'],
                             2)
                    + math.pow(self.config.csettings['vessel_configuration'][vessel_name]['gps_offset_y']
                                   + self.config.csettings['vessel_configuration'][vessel_name]['vrp_offset_y'],
                               2))  # Distance in m

                vessel_lon, vessel_lat = endpoint_sphere(vessel_lon, vessel_lat, distance,
                                                             math.degrees(brng))

                self.socketio.emit('new_vessel_position', {'latitude': vessel_lat,
                                                          'longitude': vessel_lon,
                                                          'heading': vessel_heading,
                                                          'vessel_width': vessel_width,
                                                          'vessel_length': vessel_length}, namespace='/viewer')
            else:
                self.socketio.emit('vessel_no_signal', {}, namespace='/viewer')


            if (self.usbl_data.time is not None and self.usbl_data.time != 0.0):
                usbl_lat = self.usbl_data.latitude
                usbl_lon = self.usbl_data.longitude
                # usbl_heading = self.usbl_data['heading']
                usbl_heading = 0
                usbl_depth = self.usbl_data.depth
                self.socketio.emit('new_usbl_position', {'latitude': usbl_lat,
                                                    'longitude': usbl_lon,
                                                    'heading': usbl_heading,
                                                    'depth': usbl_depth}, namespace='/viewer')
            else:
                self.socketio.emit('usbl_no_signal', {}, namespace='/viewer')

            self.socketio.sleep(1)


    def update_gps_data(self, gps_position: GPSPosition, gps_orientation: GPSOrientation):
        self.gps_position = gps_position
        self.gps_orientation = gps_orientation

    def update_usbl_data(self, usbl_data):
        self.usbl_data = usbl_data

    def update_auv_data(self, auv_data):
        self.auv_data = auv_data

    @staticmethod
    @app.route('/')
    def index():
        # only by sending this page first will the client be connected to the socketio instance
        return render_template('index.html')

    # @socketio.on('connect', namespace='/viewer')
    def connect(self):
        # need visibility of the global thread object
        logger.info('Client connected')

        #Start the position generator thread only if the thread has not been started before.
        if self.thread is None:
            self.thread = self.socketio.start_background_task(self.position_generator)


    # @socketio.on('disconnect', namespace='/viewer')
    def disconnect(self):
        logger.info('Client disconnected')
