"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to customize ros parameters, checklists and mission actions
"""
import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget

from iquaview.src.vehicle.rosparamsconfigwidget import RosParamsConfigWidget
from iquaview.src.mission.missionactionsconfigwidget import MissionActionsConfigWidget
from iquaview.src.vehicle.checklist.checklistsconfigwidget import CheckListsWidget
from iquaview.src.ui.ui_customizationsettingswidget import Ui_CustomizationSettingsWidget

logger = logging.getLogger(__name__)


class CustomizationSettings(QWidget, Ui_CustomizationSettingsWidget):
    refresh_signal = pyqtSignal()

    def __init__(self, config, vehicle_info):
        super(CustomizationSettings, self).__init__()
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info
        self.ros_params = RosParamsConfigWidget(config, self.vehicle_info)
        self.mission_actions = MissionActionsConfigWidget(config)
        self.check_lists = CheckListsWidget(config)
        self.rosparam_widget.layout().addWidget(self.ros_params)
        self.rosparam_widget.layout().addWidget(self.mission_actions)
        self.rosparam_widget.layout().addWidget(self.check_lists)

    def reload(self):
        """
        Reload widgets
        """
        self.ros_params.load_ros_params()
        self.mission_actions.load_mission_actions()
        self.check_lists.load_check_lists()

    def on_accept(self):
        """ Save settings"""
        self.ros_params.save()
        self.mission_actions.save()
        self.check_lists.save()