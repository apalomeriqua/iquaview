# Changelog
All notable changes to IQUAview are documented in this file.

## [20.10.8] - 2021-09-06
### Added
* Support for Iqua Strobe Lights.

## [20.10.7] - 2021-05-31
### Added
* Support for Norbit WBMS Multibeam.

## [20.10.6] - 2021-04-13
### Fixed
* CRS transformation when adding a new landmark.

## [20.10.5] - 2021-04-06
### Fixed
* Rendering problem when clicking on waypoints with the edit tool.

## [20.10.4] - 2021-03-26
### Added
* Support for KVH GEOFOG3D INS.

### Fixed
* Added support for Qgis version 3.16 to fix mission editing issue.

## [20.10.3] - 2021-03-17
### Fixed
* The new version of socket.io (3.1.3) is used on the client side of the web server to maintain compatibility.
* The gevent-websocket libraries are used to improve performance.
* Bug fix when some values are saved as default.

## [20.10.2] - 2021-03-04
### Fixed
* The message when editing the mission.
* The order in which icons are displayed on the canvas.

## [20.10.1] - 2021-01-15
### Fixed
* Bug fix in the temporary data structure.

## [20.10.0] - 2020-10-23
### Added

* Support for S-57 (ENC) vector layers.
* Option to create layer groups.
* Widget to edit customization settings in options menu (no need to manually edit the XML from now on).
* Lock/unlock buttons in track widgets to keep the map centered on the track extent.
* Waypoint table view for summarizing mission waypoints.
* Integration of web server for displaying basic canvas view and AUV/USBL positions.
* Functionality to download bagfiles.
* New star template for missions.
* Display of message describing recovery action reason in message bar. 
* New plugin for FLIR Spinnaker Camera (upon request). 

### Changed

* GNSS position and heading are set separately.
* Each AUV parameter has an independent reload service associated in the configuration XML.
* Last canvas position and zoom is set on opening.

### Removed

* Removed backward compatibility with COLA2 releases prior to 201902.

### Fixed
* Minor bugs.

## [1.2.1] - 2020-8-26

* Added support for Qgis version 3.10.8

## [1.2.0] - 2019-10-26
### Added

* Method to check if all actions in a mission are present in the config XML.
* Multiple missions can be loaded at once.
* Vehicle roslaunch list added to XML configuration.
* Support to import CSV layers.
* Selector to change Project Coordinate Reference System (CRS).
* World map layer as default layer.
* Buttons to pause and resume a mission.
* Drag&Drop functionallity to import missions and layers (vector and raster) to the project.
* SFTP client to get and transfer files from/to the AUV.
* Undo (ctrl+z) functionallity when moving and rotating missions.
* Show coordinates of single landmark points by right clicking them in the layer legend.
* Now hovering the mouse over a mission or a vector layer displays the name of that layer in a tooltip.
* Button to move NED Origin.
* Menu to export positions through serial or ethernet.
### Changed
* Tracks remain on the canvas even if the connection is lost.
* Rotation key changed from Ctrl to Shift.
* Service calls are handled as Trigger services instead of Empty services.
* Re-styling of battery and thrusters widgets.
* Re-styling of options menu.
### Fixed
* Minor bugs

## [1.1.0] - 2019-02-26

* First Release version.

## [1.0.0] - 2017-07-30

* Initial version.
