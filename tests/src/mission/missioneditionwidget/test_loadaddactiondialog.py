"""
Copyright (c) 2018 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest
from copy import copy

from PyQt5.QtWidgets import QApplication
from iquaview_lib.config import Config
from iquaview.src.mission.missioneditionwidget.loadaddactiondialog import Loadaddactiondialog

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestLoadAddActionDialog(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = copy(self.config.settings)

        self.add_action_dlg = Loadaddactiondialog(self.config)

    def test(self):
        self.assertIsNot(None, self.add_action_dlg.get_action_name())
        self.assertIsNot(None, self.add_action_dlg.get_action_id())
        self.assertIsNot(None, self.add_action_dlg.get_description())
        self.assertIsNot(None, self.add_action_dlg.get_params())

        index = self.add_action_dlg.comboBox.currentIndex()
        self.add_action_dlg.comboBox.setCurrentIndex(index+1)


if __name__ == "__main__":
    unittest.main()
