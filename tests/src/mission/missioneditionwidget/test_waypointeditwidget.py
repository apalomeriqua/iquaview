"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest
from copy import copy

from PyQt5.QtCore import Qt
from PyQt5.QtTest import QTest
from PyQt5.QtWidgets import QMessageBox, QDialog

from iquaview.src.mission.missioneditionwidget.loadaddactiondialog import Loadaddactiondialog
from iquaview.src.mission.missioneditionwidget.waypointeditwidget import WaypointEditWidget

from PyQt5.QtWidgets import QApplication
from qgis.core import QgsProject, QgsApplication
from qgis.gui import QgsMapCanvas


from iquaview_lib.config import Config
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.vehicle.checklist.loadcheckactiondlg import LoadCheckActionDialog
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.mission.missioncontroller import MissionController
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 Parameter,
                                                 MissionAction,
                                                 MissionSection,
                                                 MissionPark,
                                                 MissionGoto)

from unittest.mock import Mock

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestWaypointEditWidget(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.config = Config()
        self.config.load()
        self.config.csettings = copy(self.config.settings)

        self.vehicle_info = VehicleInfo(self.config)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()

        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)

        self.mission_name = "temp_edittool_mission"
        self.write_temp_mission_xml()
        self.mission_ctrl.load_mission(os.getcwd()+"/"+self.mission_name + ".xml")

        self.mission_track = self.mission_ctrl.mission_list[0]
        self.vehicle_name = self.vehicle_info.get_vehicle_namespace()

        # Mock warnings so no warning dialogs are actually created
        QMessageBox.warning = Mock()

    def tearDown(self):
        os.remove(self.mission_name + ".xml")

    def test_single_edition(self):
        widget = WaypointEditWidget(self.config, self.mission_track, self.vehicle_name, multiple_edition=False)

        # z = 0 in altitude mode, forcing a warning
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(0.0)

        # test different depth values
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(-1)

        # Deactivate altitude mode
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # test different z values
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(0.0)

        # test different coordinates
        widget.latitude_lineEdit.setText("0.0")
        widget.longitude_lineEdit.setText("0.0")
        widget.latitude_lineEdit.setText("1")
        widget.longitude_lineEdit.setText("-1")
        widget.latitude_lineEdit.setText("-1")
        widget.longitude_lineEdit.setText("1")
        widget.latitude_lineEdit.setText("75")
        widget.longitude_lineEdit.setText("75")
        widget.latitude_lineEdit.setText("-75")
        widget.longitude_lineEdit.setText("-75")
        widget.latitude_lineEdit.setText("41")
        widget.longitude_lineEdit.setText("3")

        # test different speeds
        widget.speed_doubleSpinBox.setValue(0.0)
        widget.speed_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.speed_doubleSpinBox.setValue(1)
        widget.speed_doubleSpinBox.setValue(2)
        widget.speed_doubleSpinBox.setValue(0.2)
        widget.speed_doubleSpinBox.setValue(0.5)

        # test different tolerances
        widget.tolerance_xy_doubleSpinBox.setValue(0.0)  # this should cause warning, it doesn't
        widget.tolerance_xy_doubleSpinBox.setValue(0.5)
        widget.tolerance_xy_doubleSpinBox.setValue(1)
        widget.tolerance_xy_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.tolerance_xy_doubleSpinBox.setValue(2)

        # Check buttons
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)

        # Check that mission_track remove_step is called when "delete waypoint" button is pressed
        # temporary store real method
        real_method = MissionTrack.remove_step
        MissionTrack.remove_step = Mock()
        QTest.mouseClick(widget.delete_pushButton, Qt.LeftButton)
        self.assertTrue(MissionTrack.remove_step.called)

        # restore remove_step method
        MissionTrack.remove_step = real_method

        # Changing maneuver type
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.on_heave_mode_box_changed(2)
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # Add action
        Loadaddactiondialog.exec_ = Mock(return_value=QDialog.Accepted)
        QTest.mouseClick(widget.addAction_pushButton, Qt.LeftButton)

        # Remove action
        QTest.mouseClick(widget.removeAction_pushButton, Qt.LeftButton)

    def test_multiple_edition(self):
        widget = WaypointEditWidget(self.config, self.mission_track, self.vehicle_name, multiple_edition=True)
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # Select all waypoints from the mission
        waypoints = list(range(0, self.mission_track.get_mission_length()))
        widget.show_multiple_features(waypoints)

        # z = 0 in altitude mode, forcing a warning
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.set_altitude(0.0)

        # test different z values
        widget.heave_mode_widget.set_altitude(1)
        widget.heave_mode_widget.set_altitude(99)
        widget.heave_mode_widget.set_altitude(3.14)
        widget.heave_mode_widget.set_altitude(-1)

        # Deactivate altitude mode
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # test different z values
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(0.0)

        # test different coordinates
        widget.latitude_lineEdit.setText("0.0")
        widget.longitude_lineEdit.setText("0.0")
        widget.latitude_lineEdit.setText("1")
        widget.longitude_lineEdit.setText("-1")
        widget.latitude_lineEdit.setText("-1")
        widget.longitude_lineEdit.setText("1")
        widget.latitude_lineEdit.setText("75")
        widget.longitude_lineEdit.setText("75")
        widget.latitude_lineEdit.setText("-75")
        widget.longitude_lineEdit.setText("-75")
        widget.latitude_lineEdit.setText("41")
        widget.longitude_lineEdit.setText("3")

        # test different speeds
        widget.speed_doubleSpinBox.setValue(0.0)
        widget.speed_doubleSpinBox.setValue(1)
        widget.speed_doubleSpinBox.setValue(2)
        widget.speed_doubleSpinBox.setValue(0.2)
        widget.speed_doubleSpinBox.setValue(0.5)

        # test different tolerances
        widget.tolerance_xy_doubleSpinBox.setValue(0.0)  # this should cause warning, it doesn't
        widget.tolerance_xy_doubleSpinBox.setValue(0.5)
        widget.tolerance_xy_doubleSpinBox.setValue(1)
        widget.tolerance_xy_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.tolerance_xy_doubleSpinBox.setValue(2)

        # Check change waypoint buttons
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)

        # Check that mission_track remove_step is called when "delete waypoint" button is pressed
        # temporary store real method
        real_method = MissionTrack.remove_step
        MissionTrack.remove_step = Mock()
        QTest.mouseClick(widget.delete_pushButton, Qt.LeftButton)
        self.assertTrue(MissionTrack.remove_step.called)

        # restore remove_step method
        MissionTrack.remove_step = real_method

        # Changing maneuver type
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.show_multiple_features(waypoints)

        widget.heave_mode_widget.on_heave_mode_box_changed(2)
        widget.show_multiple_features(waypoints)

        widget.heave_mode_widget.on_heave_mode_box_changed(0)
        widget.show_multiple_features(waypoints)


        # Add action
        Loadaddactiondialog.exec_ = Mock(return_value=QDialog.Accepted)
        QTest.mouseClick(widget.addAction_pushButton, Qt.LeftButton)

        # Remove action
        QTest.mouseClick(widget.removeAction_pushButton, Qt.LeftButton)

    def write_temp_mission_xml(self):
        mission = Mission()
        mission_step = MissionStep()
        param = Parameter("abcd")
        param_2 = Parameter("2")
        parameters = list()
        parameters.append(param)
        parameters.append(param_2)
        action = MissionAction("action1", parameters)
        mission_step.add_action(action)
        wp = MissionGoto(41.777, 3.030, 15.0, 0.0,
                         0, 0.5,2.0, True)
        mission_step.add_maneuver(wp)
        mission.add_step(mission_step)
        mission_step2 = MissionStep()
        sec = MissionSection(41.777, 3.030, 15.0,
                             41.777, 3.030, 15.0, 0.0,
                             0, 0.5, 2.0, True)
        mission_step2.add_maneuver(sec)
        mission.add_step(mission_step2)
        mission_step3 = MissionStep()
        park = MissionPark(41.777, 3.030, 15.0, 0.0,
                           0.5, True,
                           0, 0.5, 120, True)
        mission_step3.add_maneuver(park)
        mission.add_step(mission_step3)
        mission.write_mission(self.mission_name + '.xml')
