#!/usr/bin/env python

from distutils.core import setup


setup(
    name="iquaview",
    version="20.10.8",
    author="Iqua Robotics",
    author_email="software@iquarobotics.com",
    description="IQUA Robotics graphical user interface",
    license="Read LICENSE.txt",
    packages=['iquaview',
              'iquaview.src',
              'iquaview.src.canvastracks',
              'iquaview.src.connection',
              'iquaview.src.connection.settings',
              'iquaview.src.mapsetup',
              'iquaview.src.mapsetup.decoration',
              'iquaview.src.mission',
              'iquaview.src.mission.maptools',
              'iquaview.src.mission.missioneditionwidget',
              'iquaview.src.mission.missiontemplates',
              'iquaview.src.plugins',
              'iquaview.src.plugins.flir_spinnaker_camera',
              'iquaview.src.plugins.imagenex_deltat_multibeam',
              'iquaview.src.plugins.imagenex_sidescan',
              'iquaview.src.plugins.iqua_strobe_lights',
              'iquaview.src.plugins.kvh_geofog3d_ins',
              'iquaview.src.plugins.norbit_wbms_multibeam',
              'iquaview.src.plugins.USBL',
              'iquaview.src.tools',
              'iquaview.src.tools.web_server',
              'iquaview.src.ui',
              'iquaview.src.vehicle',
              'iquaview.src.vehicle.checklist',
              'iquaview.src.vehicle.vehiclewidgets'],
    include_package_data=True,
    package_data={"iquaview": ['*.xml']},
    entry_points={
        'gui_scripts': ['iquaview = iquaview.src.__main__:main'],
    },
    data_files=[
        ('share/applications/', ['iquaview.desktop']),
        ('share/icons/hicolor/scalable/apps/', ['iquaview/src/resources/iquaview.png']),
    ],
    install_requires=[
        'matplotlib',
        'lxml',
        'pysftp',
        'geopy',
        'dataclasses',
        'Flask',
        'Flask-SocketIO',
        'gevent',
        'gevent-websocket'
    ],
)